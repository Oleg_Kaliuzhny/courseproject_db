﻿using BLL.Domains;
using System;
using DAL.Domains.Repositories;
using DAL.Domains.DTO;

namespace ConsoleUI
{
    class Program
    {
        static void Main(string[] args)
        {
            Nodes nodes = new Nodes(new NodeRepository(), new ClientRepository(), new VehicleRepository(),
                new ContractRepository(), new CategoryRepository(), new AddressRepository());

            var n = nodes.GetNodes(0, 50);

            foreach (var item in n)
            {
                Console.WriteLine(item.Category.Name + " " + item.Client.FirstName + " " + item.Contract.ContractNumber + " " + item.Vehicle.Brand);
            }
            
            Console.ReadKey();
        }
    }
}
