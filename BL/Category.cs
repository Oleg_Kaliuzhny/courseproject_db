﻿using BL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public class Category : ICategory
    {
        public Category(IDataList dataList, string name)
        {
            this._name = name;
            this._dataList = dataList;
        }

        IDataList _dataList;

        private string _name;

        public string Name
        {
            get { return _name; }
            //set { _name = value; }
        }

        public int AmountOfData
        {
            get { return _dataList.AmountOfDataInCategory(this._name); }
        }

        public override string ToString()
        {
            return _name;
        }

    }
}
