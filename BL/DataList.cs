﻿using BL.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Xml.Serialization;

namespace BL
{
    public class DataList : IDataList
    {
        #region Конструктор
        public DataList()
        {
            this.ResetAll();
        }
        #endregion

        #region Свойства
        private List<Data> _listOfData;
        /// <summary>
        /// Список данных (связки клиент-транспорт-контракт)
        /// </summary>
        public List<Data> ListOfData
        {
            get { return _listOfData; }
            private set { _listOfData = value; }
        }

        private List<string> _categories;
        /// <summary>
        /// Список категорий
        /// </summary>
        public List<string> Categories
        {
            get { return _categories; }
            private set { _categories = value; }
        }

        //private List<IClient> _clients;
        /// <summary>
        /// Список всех клиентов
        /// </summary>
        public List<Client> Clients
        {
            get
            {
                return new List<Client>(this.ListOfData.Select(x => x.Client));
            }
            //set { _clients = value; }
        }

        //private List<IVehicle> _vehicles;
        /// <summary>
        /// Список всего транспорта
        /// </summary>
        public List<Vehicle> Vehicles
        {
            get
            {
                return new List<Vehicle>(this.ListOfData.Select(x => x.Vehicle));
            }
            //set { _vehicles = value; }
        }

        //private List<IContract> _contracts;
        /// <summary>
        /// Список всех контрактов
        /// </summary>
        public List<Contract> Contracts
        {
            get
            {
                return new List<Contract>(this.ListOfData.Select(x => x.Contract));
            }
            //set { _contracts = value; }
        }

        #endregion

        #region Методы
        public void ResetAll()
        {
            _listOfData = new List<Data>();
            _categories = new List<string>();
        }

        public void AddData(Client client, Vehicle vehicle, Contract contract, string category)
        {
            _listOfData.Add(new Data(client, vehicle, contract, category));
            if (!_categories.Contains(category))
                _categories.Add(category);
            this.DataAdded?.Invoke();
        }

        public void AddData(Data data)
        {
            _listOfData.Add(data);
            this.DataAdded?.Invoke();
        }

        public void RemoveData(Data data)
        {
            if (_listOfData.Contains(data))
            {
                this._listOfData.Remove(data);
                this.DataRemoved?.Invoke();
            }
            else
            {
                this.DataNotFound?.Invoke();
            }
        }

        public void AddCategory(string categoryName)
        {
            _categories.Add(categoryName);
            this.CategoryAdded?.Invoke();
        }

        public void RemoveCategory(string categoryName)
        {
            if (_categories.Contains(categoryName))
            {
                int i = 0;
                bool flag = false;
                while (i < ListOfData.Count)
                {
                    if (ListOfData[i].Category == categoryName)
                    {
                        ListOfData.RemoveAt(i);
                        flag = true;
                    }
                    else
                        flag = false;
                    if (!flag)
                        i++;
                }

                this._categories.Remove(categoryName);

                this.CategoryRemoved?.Invoke();
            }
            else
            {
                this.CategoryNotFound?.Invoke();
            }
        }

        public int AmountOfDataInCategory(string categoryName)
        {
            int counter = 0;
            foreach (Data item in this.ListOfData)
                if (item.Category == categoryName)
                    counter++;
            return counter;
        }

        public List<Data> GetAllDataFromCategory(string categoryName)
        {
            return this.ListOfData.FindAll(i => i.Category == categoryName);
        }

        public bool RenameCategory(string oldName, string newName)
        {
            if (oldName == newName)
                return false;

            foreach (Data item in this.ListOfData)
                if (item.Category == oldName)
                    item.Category = newName;

            if (this.Categories.Contains(newName))
                this.Categories.RemoveAt(this.Categories.IndexOf(oldName));
            else
                this.Categories[this.Categories.IndexOf(oldName)] = newName;

            return true;
        }

        public string[] GetAllCategories()
        {
            return Categories.ToArray();
        }

        public void Save()
        {
            if (File.Exists("ListOfData.xml"))
            {
                File.Delete("ListOfData.xml");
            }

            if (File.Exists("Categories.xml"))
            {
                File.Delete("Categories.xml");
            }

            XmlSerializer formatter = new XmlSerializer(typeof(List<Data>));
            using (FileStream fs = new FileStream("ListOfData.xml", FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, ListOfData);
            }

            XmlSerializer formatter2 = new XmlSerializer(typeof(List<string>));
            using (FileStream fs = new FileStream("Categories.xml", FileMode.OpenOrCreate))
            {
                formatter2.Serialize(fs, Categories);
            }
            this.Saved?.Invoke();
        }

        public void Load()
        {
            using (FileStream fs = new FileStream("ListOfData.xml", FileMode.OpenOrCreate))
            {
                XmlSerializer formatter = new XmlSerializer(typeof(List<Data>));
                ListOfData = (List<Data>)formatter.Deserialize(fs);
            }

            using (FileStream fs = new FileStream("Categories.xml", FileMode.OpenOrCreate))
            {
                XmlSerializer formatter = new XmlSerializer(typeof(List<string>));
                Categories = (List<string>)formatter.Deserialize(fs);
            }
            this.Loaded?.Invoke();
        }

        //поиск
        public List<Data> SearchByKeywordAmongClients(List<Data> list, string keyword)
        {
            List<Data> result = list.FindAll(i => (i.Client.LastName.Contains(keyword) ||
            i.Client.Address.Contains(keyword) ||
            i.Client.DriversLicense.Contains(keyword) ||
            i.Client.Name.Contains(keyword)) ||
            i.Client.PassportNumber.Contains(keyword) ||
            i.Client.Patronymic.Contains(keyword) ||
            i.Client.Phone.Contains(keyword) ||
            i.Client.CodeID.ToString().Contains(keyword) ||
            i.Client.DriversExpirience.ToString().Contains(keyword));
            //SearchFinished?.Invoke(result);
            return result;
        }

        public List<Data> SearchByKeywordAmongVehicles(List<Data> list, string keyword)
        {
            List<Data> result = list.FindAll(i => (i.Vehicle.Brand.Contains(keyword) ||
            i.Vehicle.Number.ToString().Contains(keyword) ||
            i.Vehicle.Year.ToString().Contains(keyword) ||
            i.Vehicle.LicensePlate.Contains(keyword)) ||
            i.Vehicle.Value.ToString().Contains(keyword) ||
            i.Vehicle.Color.Contains(keyword) ||
            i.Vehicle.Volume.ToString().Contains(keyword) ||
            i.Vehicle.AmountOfSeats.ToString().Contains(keyword) ||
            i.Vehicle.CityOfRegistration.Contains(keyword) ||
            i.Vehicle.Type.Contains(keyword) ||
            i.Vehicle.Carrying.ToString().Contains(keyword));
            //SearchFinished?.Invoke(result);
            return result;
        }

        public List<Data> SearchByAllContractsInRangeOfValue(List<Data> list, double min, double max)
        {
            return list.FindAll(x => x.Contract.Value >= min && x.Contract.Value <= max);
        }

        public List<Data> SearchByVehiclesValuesLessThan(List<Data> list, double value)
        {
            List<Data> result = list.FindAll(x => x.Vehicle.Value < value);
            //SearchFinished?.Invoke(result);
            return result;
        }

        public List<Data> SearchByKeywordAmongData(List<Data> list, string keyword)
        {
            return list.FindAll(i => (i.Client.ToString().Contains(keyword) || i.Vehicle.ToString().Contains(keyword) || i.Contract.ToString().Contains(keyword)));
        }

        public string[] FindInCategories(string[] categories, string searchQuery)
        {
            return (categories != null && !string.IsNullOrEmpty(searchQuery)) ? categories.Where((str) => str.Contains(searchQuery)).ToArray() : categories;
        }

        #endregion

        #region События
        public event Action DataNotFound;
        public event Action DataAdded;
        public event Action DataRemoved;
        public event Action CategoryAdded;
        public event Action CategoryNotFound;
        public event Action CategoryRemoved;
        public event Action Saved;
        public event Action Loaded;
        public event Action<List<Data>> SearchFinished;
        #endregion
    }
}
