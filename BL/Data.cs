﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL.Interfaces;

namespace BL
{
    [Serializable]
    public class Data : IData
    {
        #region Конструктор
        public Data() { }

        public Data (Client client, Vehicle vehicle, Contract contract, string category)
        {
            _client = client;
            _vehicle = vehicle;
            _contract = contract;
            _category = category;
        }
        #endregion
        #region Свойства
        private Client _client;
        /// <summary>
        /// Клиент
        /// </summary>
        public Client Client
        {
            get { return _client; }
            set { _client = value; }
        }

        private Vehicle _vehicle;
        /// <summary>
        /// Транспорт
        /// </summary>
        public Vehicle Vehicle
        {
            get { return _vehicle; }
            set { _vehicle = value; }
        }

        private Contract _contract;
        /// <summary>
        /// Контракт
        /// </summary>
        public Contract Contract
        {
            get { return _contract; }
            set { _contract = value; }
        }

        private string _category;
        /// <summary>
        /// Категория
        /// </summary>
        public string Category
        {
            get { return _category; }
            set { _category = value; }
        }

        #endregion
        #region Методы
        /// <summary>
        /// Заменить категорию
        /// </summary>
        /// <param name="newCategory">Новая категория для замены</param>
        public void ReplaceCategory(string newCategory)
        {
            this._category = newCategory;
            this.CategoryReplaced?.Invoke();
        }

        /// <summary>
        /// Заменить клиента
        /// </summary>
        /// <param name="newClient">Новый клиент для замены</param>
        public void ReplaceClient(Client newClient)
        {
            this._client = newClient;
            this.ClientReplaced?.Invoke();
        }

        /// <summary>
        /// Заменить транспорт
        /// </summary>
        /// <param name="newVehicle">Новый транспорт</param>
        public void ReplaceVehicle(Vehicle newVehicle)
        {
            this._vehicle = newVehicle;
            this.VehicleReplaced?.Invoke();
        }

        /// <summary>
        /// Заменить контракт
        /// </summary>
        /// <param name="newContract">Новый контракт</param>
        public void ReplaceContract(Contract newContract)
        {
            this._contract = newContract;
            this.ContractReplaced?.Invoke();
        }

        #endregion
        #region События
        public event Action CategoryReplaced;
        public event Action ClientReplaced;
        public event Action VehicleReplaced;
        public event Action ContractReplaced;
        #endregion
    }
}
