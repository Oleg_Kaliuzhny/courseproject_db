﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Interfaces
{
    public interface IData
    {
        Client Client { get; }
        Vehicle Vehicle { get; }
        Contract Contract { get; }
        string Category { get; }

        void ReplaceCategory(string newCategory);
        void ReplaceClient(Client newClient);
        void ReplaceVehicle(Vehicle newVehicle);
        void ReplaceContract(Contract newContract);

        event Action CategoryReplaced;
        event Action ClientReplaced;
        event Action VehicleReplaced;
        event Action ContractReplaced;
    }
}
