﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Interfaces
{
    public interface IDataList
    {
        List<Data> ListOfData { get; }
        List<string> Categories { get; }
        //
        List<Client> Clients { get; }
        List<Vehicle> Vehicles { get; }
        List<Contract> Contracts { get; }
        //
        void ResetAll();
        void AddData(Client client, Vehicle vehicle, Contract contract, string category);
        void AddData(Data data);
        void RemoveData(Data data);
        //
        void AddCategory(string categoryName);
        void RemoveCategory(string categoryName);
        void Save();
        void Load();
        string[] GetAllCategories();
        List<Data> GetAllDataFromCategory(string categoryName);
        int AmountOfDataInCategory(string categoryName);
        string[] FindInCategories(string[] categories, string searchQuery);
        bool RenameCategory(string oldName, string newName);
        //поиск
        List<Data> SearchByKeywordAmongClients(List<Data> list, string keyword);
        List<Data> SearchByKeywordAmongVehicles(List<Data> list, string keyword);
        List<Data> SearchByAllContractsInRangeOfValue(List<Data> list, double min, double max);
        List<Data> SearchByVehiclesValuesLessThan(List<Data> list, double value);
        List<Data> SearchByKeywordAmongData(List<Data> list, string keyword);
        //
        event Action DataNotFound;
        event Action DataAdded;
        event Action DataRemoved;
        event Action CategoryAdded;
        event Action CategoryNotFound;
        event Action CategoryRemoved;
        event Action Saved;
        event Action Loaded;
        event Action<List<Data>> SearchFinished;
    }
}
