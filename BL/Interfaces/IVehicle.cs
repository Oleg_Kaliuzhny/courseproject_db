﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Interfaces
{
    public interface IVehicle
    {
        string Brand { get; set; }
        int Number { get; set; }
        int Year { get; set; }
        string LicensePlate { get; set; }
        double Value { get; set; }
        string Color { get; set; }
        double Volume { get; set; }
        int AmountOfSeats { get; set; }
        double Carrying { get; set; }
        string CityOfRegistration { get; set; }
        string Type { get; set; }
        string ToString();
    }
}
