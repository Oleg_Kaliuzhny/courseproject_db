﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Interfaces
{
    public interface IContract
    {
        double ContractNumber { get; set; }
        int CodeID { get; set; }
        bool Flag { get; set; }
        string Brand { get; set; }
        double Value { get; set; }
        string OwnerName { get; set; }
        double Franchise { get; set; }
        DateTime BeginningDate { get; set; }
        DateTime FinishingDate { get; set; }
        DateTime RegistrationDate { get; set; }
        int Sticker { get; set; }
        string ToString();
    }
}
