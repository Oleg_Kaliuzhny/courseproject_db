﻿namespace BL.Interfaces
{
    public interface ICategory
    {
        int AmountOfData { get; }
        string Name { get; }

        string ToString();
    }
}