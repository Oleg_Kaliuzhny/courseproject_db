﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Interfaces
{
    public interface IClient
    {
        string Name { get; set; }
        string LastName { get; set; }
        string Patronymic { get; set; }
        int CodeID { get; set; }
        bool Flag { get; set; }
        string PassportNumber { get; set; }
        string Address { get; set; }
        string Phone { get; set; }
        string DriversLicense { get; set; }
        int DriversExpirience { get; set; }
        string ToString();
    }
}
