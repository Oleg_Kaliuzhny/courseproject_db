﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL.Interfaces;

namespace BL
{
    [Serializable]
    public class Vehicle : IComparable, IVehicle
    {
        #region Конструктор
        public Vehicle() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Brand">Марка транспортного средства</param>
        /// <param name="Number">Номер кузова транспортного средства</param>
        /// <param name="Year">Год выпуска транспортного средства</param>
        /// <param name="LicensePlate">Государственный номерной знак</param>
        /// <param name="Value">Стоимость транспортного средства</param>
        /// <param name="Color">Цвет транспортного средства</param>
        /// <param name="Volume">Объем двигателя транспортного средства</param>
        /// <param name="AmountOfSeats">Количество мест в транспортном средстве</param>
        /// <param name="Carrying">Грузоподъемность транспортного средства</param>
        /// <param name="CityOfRegistration">Город регистрации транспортного средства</param>
        /// <param name="Type">Тип транспортного средства</param>
        public Vehicle(string brand ,
        int number ,
        int year ,
        string licensePlate ,
        double value ,
        string color ,
        double volume ,
        int amountOfSeats,
        double carrying ,
        string cityOfRegistration,
        string type)
        {
            _brand = brand;
            _number = number;
            _year = year;
            _licensePlate = licensePlate;
            _value = value;
            _color = color;
            _volume = volume;
            _amountOfSeats = amountOfSeats;
            _carrying = carrying;
            _cityOfRegistration = cityOfRegistration;
            _type = type;
        }
        #endregion
        #region Свойства
        private string _brand;
        /// <summary>
        /// Марка транспортного средства
        /// </summary>
        public string Brand
        {
            get { return _brand; }
            set { _brand = value; }
        }

        private int _number;
        /// <summary>
        /// Номер кузова транспортного средства
        /// </summary>
        public int Number
        {
            get { return _number; }
            set { _number = value; }
        }

        private int _year;
        /// <summary>
        /// Год выпуска транспортного средства
        /// </summary>
        public int Year
        {
            get { return _year; }
            set { _year = value; }
        }

        private string _licensePlate;
        /// <summary>
        /// Государственный номерной знак
        /// </summary>
        public string LicensePlate
        {
            get { return _licensePlate; }
            set { _licensePlate = value; }
        }

        private double _value;
        /// <summary>
        /// Стоимость транспортного средства
        /// </summary>
        public double Value
        {
            get { return _value; }
            set { _value = value; }
        }

        private string _color;
        /// <summary>
        /// Цвет транспортного средства
        /// </summary>
        public string Color
        {
            get { return _color; }
            set { _color = value; }
        }

        private double _volume;
        /// <summary>
        /// Объем двигателя транспортного средства
        /// </summary>
        public double Volume
        {
            get { return _volume; }
            set { _volume = value; }
        }

        private int _amountOfSeats;
        /// <summary>
        /// Количество мест в транспортном средстве
        /// </summary>
        public int AmountOfSeats
        {
            get { return _amountOfSeats; }
            set { _amountOfSeats = value; }
        }

        private double _carrying;
        /// <summary>
        /// Грузоподъемность транспортного средства
        /// </summary>
        public double Carrying
        {
            get { return _carrying; }
            set { _carrying = value; }
        }

        private string _cityOfRegistration;
        /// <summary>
        /// Город регистрации транспортного средства
        /// </summary>
        public string CityOfRegistration
        {
            get { return _cityOfRegistration; }
            set { _cityOfRegistration = value; }
        }

        private string _type;
        /// <summary>
        /// Тип транспорта
        /// </summary>
        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }

        #endregion
        #region Методы
        public override string ToString()
        {
            return _brand;
        }

        public int CompareTo(object obj)
        {
            return this.ToString().CompareTo((obj as Vehicle).ToString());
        }

        #endregion
    }
}
