﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL.Interfaces;

namespace BL
{
    [Serializable]
    public class Client : IComparable, IClient
    {
        #region Конструктор
        public Client() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Name">Имя клиента</param>
        /// <param name="LastName">Фамилия клиента</param>
        /// <param name="Patronymic">Отчество клиента</param>
        /// <param name="CodeID">Идентификационный код</param>
        /// <param name="Flag">Является ли физической особью</param>
        /// <param name="PassportNumber">Серия и номер паспорта</param>
        /// <param name="Address">Адрес клиента</param>
        /// <param name="Phone">Номер телефона</param>
        /// <param name="DriversLicense">Серия и номер водительского удостоверения</param>
        /// <param name="DriversExpirience">Водительский опыт</param>
        public Client(string name ,
        string lastName ,
        string patronymic ,
        int codeID ,
        bool flag ,
        string passportNumber ,
        string address ,
        string phone ,
        string driversLicense ,
        int driversExpirience)
        {
            _name = name;
            _lastName = lastName;
            _patronymic = patronymic;
            _codeID = codeID;
            _flag = flag;
            _passportNumber = passportNumber;
            _address = address;
            _phone = phone;
            _driversLicense = driversLicense;
            _driversExpirience = driversExpirience;
        }
        #endregion
        #region Свойства
        private int _codeID;
        /// <summary>
        /// Идентификационный код
        /// </summary>
        public int CodeID
        {
            get { return _codeID; }
            set { _codeID = value; }
        }

        private string _lastName;
        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        private bool _flag;
        /// <summary>
        /// Является ли физической особью
        /// </summary>
        public bool Flag
        {
            get { return _flag; }
            set { _flag = value; }
        }

        private string _name;
        /// <summary>
        /// Имя клиента
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _patronymic;
        /// <summary>
        /// Отчество клиента
        /// </summary>
        public string Patronymic
        {
            get { return _patronymic; }
            set { _patronymic = value;  }
        }

        private string _passportNumber;
        /// <summary>
        /// Серия и номер паспорта
        /// </summary>
        public string PassportNumber
        {
            get { return _passportNumber; }
            set { _passportNumber = value; }
        }

        private string _address;
        /// <summary>
        /// Адрес клиента
        /// </summary>
        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }

        private string _phone;
        /// <summary>
        /// Телефон клиента
        /// </summary>
        public string Phone
        {
            get { return _phone; }
            set { _phone = value; }
        }

        private string _driversLicense;
        /// <summary>
        /// Серия и номер водительского удостоверения
        /// </summary>
        public string DriversLicense
        {
            get { return _driversLicense; }
            set { _driversLicense = value; }
        }

        private int _driversExpirience;
        /// <summary>
        /// Водительский опыт
        /// </summary>
        public int DriversExpirience
        {
            get { return _driversExpirience; }
            set { _driversExpirience = value; }
        }

        #endregion
        #region Методы
        public override string ToString()
        {
            return _lastName + " " + _name[0] + "." + _patronymic[0] + ".";
        }

        public int CompareTo(object obj)
        {
            return this.ToString().CompareTo((obj as Client).ToString());
        }
        #endregion
    }
}
