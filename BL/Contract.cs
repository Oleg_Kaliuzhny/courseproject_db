﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL.Interfaces;

namespace BL
{
    [Serializable]
    public class Contract : IComparable, IContract
    {
        #region Конструктор
        public Contract() { }

        public Contract(double contractNumber,
        int codeID,
        bool flag,
        string brand,
        double value,
        string ownerName,
        double franchise,
        DateTime beginningDate,
        DateTime finishingDate,
        DateTime registrationDate,
        int sticker)
        {
            _contractNumber = contractNumber;
            _codeID = codeID;
            _flag = flag;
            _brand = brand;
            _value = value;
            _ownerName = ownerName;
            _franchise = franchise;
            _beginningDate = beginningDate;
            _finishingDate = finishingDate;
            _registrationDate = registrationDate;
            _sticker = sticker;
        }
        #endregion
        #region Свойства
        private int _codeID;
        /// <summary>
        /// Идентификационный код
        /// </summary>
        public int CodeID
        {
            get { return _codeID; }
            set { _codeID = value; }
        }

        private string _lastName;
        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        private bool _flag;
        /// <summary>
        /// Является ли физической особью
        /// </summary>
        public bool Flag
        {
            get { return _flag; }
            set { _flag = value; }
        }

        private double _contractNumber;
        /// <summary>
        /// Номер договора
        /// </summary>
        public double ContractNumber
        {
            get { return _contractNumber; }
            set { _contractNumber = value; }
        }

        private string _brand;
        /// <summary>
        /// Марка транспортного средства
        /// </summary>
        public string Brand
        {
            get { return _brand; }
            set { _brand = value; }
        }

        private double _value;
        /// <summary>
        /// Стоимость транспортного средства
        /// </summary>
        public double Value
        {
            get { return _value; }
            set { _value = value; }
        }

        private string _ownerName;
        /// <summary>
        /// Фамилия и инициалы владельца
        /// </summary>
        public string OwnerName
        {
            get { return _ownerName; }
            set { _ownerName = value; }
        }

        private double _franchise;
        /// <summary>
        /// Франшиза
        /// </summary>
        public double Franchise
        {
            get { return _franchise; }
            set { _franchise = value; }
        }

        private DateTime _beginningDate;
        /// <summary>
        /// Дата начала действия договора
        /// </summary>
        public DateTime BeginningDate
        {
            get { return _beginningDate; }
            set { _beginningDate = value; }
        }

        private DateTime _finishingDate;
        /// <summary>
        /// Дата окончания действия договора
        /// </summary>
        public DateTime FinishingDate
        {
            get { return _finishingDate; }
            set { _finishingDate = value; }
        }

        private DateTime _registrationDate;
        /// <summary>
        /// Дата оформления договора
        /// </summary>
        public DateTime RegistrationDate
        {
            get { return _registrationDate; }
            set { _registrationDate = value; }
        }

        private int _sticker;
        /// <summary>
        /// Номер стикера
        /// </summary>
        public int Sticker
        {
            get { return _sticker; }
            set { _sticker = value; }
        }

        #endregion
        #region Методы
        public override string ToString()
        {
            return _contractNumber.ToString();
        }

        public int CompareTo(object obj)
        {
            return this.ToString().CompareTo((obj as Contract).ToString());
        }

        #endregion
    }
}
