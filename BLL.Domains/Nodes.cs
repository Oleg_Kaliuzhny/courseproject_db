﻿using System.Collections.Generic;
using System.Linq;
using BLL.Contracts;
using BLL.Contracts.DTO;
using BLL.Domains.DTO;
using DAL.Contracts.Repositories;

namespace BLL.Domains
{
    public class Nodes : INodes
    {
        public Nodes(INodeRepository nodeRepository,
            IClientRepository clientRepository,
            IVehicleRepository vehicleRepository,
            IContractRepository contractRepository,
            ICategoryRepository categoryRepository,
            IAddressRepository addressRepository)
        {
            _nodeRepository = nodeRepository;
            _clientRepository = clientRepository;
            _vehicleRepository = vehicleRepository;
            _contractRepository = contractRepository;
            _categoryRepository = categoryRepository;
            _addressRepository = addressRepository;
        }

        private INodeRepository _nodeRepository;
        private IClientRepository _clientRepository;
        private IVehicleRepository _vehicleRepository;
        private IContractRepository _contractRepository;
        private ICategoryRepository _categoryRepository;
        private IAddressRepository _addressRepository;

        public IEnumerable<INode> GetNodes(int page, int count)
        {
            List<INode> nodes = new List<INode>();

            IEnumerable<DAL.Contracts.DTO.INode> nodesDB = null;

            try
            {
                nodesDB = _nodeRepository.ReadNodes(page, count);
            }
            catch (System.Data.DataException)
            {
                return nodes;
            }

            if (nodesDB == null)
                return nodes;

            foreach (var node in nodesDB)
            {
                var current = TypeConterter.Convert(node);

                try
                {
                    current.Category = TypeConterter.Convert(_categoryRepository.ReadCategoryById(current.CategoryId));
                    current.Client = TypeConterter.Convert(_clientRepository.ReadClientById(current.ClientId));
                    (current.Client as DTO.Client).Address = TypeConterter.Convert(_addressRepository.ReadAddressById(current.Client.AddressId));
                    current.Contract = TypeConterter.Convert(_contractRepository.ReadContractById(current.ContractId));
                    current.Vehicle = TypeConterter.Convert(_vehicleRepository.ReadVehicleById(current.VehicleId));
                }
                catch (System.Data.DataException)
                {
                    continue;
                }

                nodes.Add(current);
            }

            return nodes;
        }

        public IEnumerable<IClient> GetClients(int page, int count)
        {
            List<IClient> clients = new List<IClient>();

            IEnumerable<DAL.Contracts.DTO.IClient> clientsBD = null;

            try
            {
                clientsBD = _clientRepository.ReadClients(page, count);
            }
            catch (System.Data.DataException)
            {
                return clients;
            }

            if (clientsBD == null)
                return clients;

            foreach (var client in clientsBD)
            {
                var current = TypeConterter.Convert(client);

                try
                {
                    current.Address = TypeConterter.Convert(_addressRepository.ReadAddressById(current.AddressId));
                }
                catch (System.Data.DataException)
                {
                    continue;
                }

                clients.Add(current);
            }

            return clients;
        }

        public IEnumerable<IVehicle> GetVehicles(int page, int count)
        {
            List<IVehicle> vehicles = new List<IVehicle>();

            IEnumerable<DAL.Contracts.DTO.IVehicle> vehiclesDB = null;

            try
            {
                vehiclesDB = _vehicleRepository.ReadVehicles(page, count);
            }
            catch (System.Data.DataException)
            {
                return vehicles;
            }

            if (vehiclesDB == null)
                return vehicles;

            foreach (var vehicle in vehiclesDB)
            {
                var current = TypeConterter.Convert(vehicle);

                vehicles.Add(current);
            }

            return vehicles;
        }

        public IEnumerable<IContract> GetContracts(int page, int count)
        {
            List<IContract> contracts = new List<IContract>();

            IEnumerable<DAL.Contracts.DTO.IContract> contractsDB = null;

            try
            {
                contractsDB = _contractRepository.ReadContracts(page, count);
            }
            catch (System.Data.DataException)
            {
                return contracts;
            }

            if (contractsDB == null)
                return contracts;

            foreach (var contract in contractsDB)
            {
                var current = TypeConterter.Convert(contract);

                contracts.Add(current);
            }

            return contracts;
        }

        public IEnumerable<ICategory> GetCategories(int page, int count)
        {
            List<ICategory> categories = new List<ICategory>();

            IEnumerable<DAL.Contracts.DTO.ICategory> categoriesDB = null;

            try
            {
                categoriesDB = _categoryRepository.ReadCategories(page, count);
            }
            catch (System.Data.DataException)
            {
                return categories;
            }

            if (categoriesDB == null)
                return categories;

            foreach (var category in categoriesDB)
            {
                var current = TypeConterter.Convert(category);

                categories.Add(current);
            }

            return categories;
        }

        public IEnumerable<INode> SearchNodes(string key, int page, int count)
        {
            return GetNodes(page, count).Where(x => x.ClientValue.ToLower().Contains(key)
            || x.ContractValue.ToLower().Contains(key)
            || x.CategoryValue.ToLower().Contains(key)
            || x.VehicleValue.ToLower().Contains(key));
        }

        public IEnumerable<IClient> SearchClients(string keyword, int page, int count)
        {
            List<IClient> clients = new List<IClient>();

            IEnumerable<DAL.Contracts.DTO.IClient> clientsBD = null;

            try
            {
                clientsBD = _clientRepository.SearchClients(keyword, page, count);
            }
            catch (System.Data.DataException)
            {
                return clients;
            }

            if (clientsBD == null)
                return clients;

            foreach (var client in clientsBD)
            {
                var current = TypeConterter.Convert(client);

                try
                {
                    current.Address = TypeConterter.Convert(_addressRepository.ReadAddressById(current.AddressId));
                }
                catch (System.Data.DataException)
                {
                    continue;
                }

                clients.Add(current);
            }

            return clients;
        }

        public IEnumerable<IVehicle> SearchVehicles(string key, double less, int page, int count)
        {
            List<IVehicle> vehicles = new List<IVehicle>();

            IEnumerable<DAL.Contracts.DTO.IVehicle> vehiclesDB = null;

            try
            {
                vehiclesDB = _vehicleRepository.SearchVehicles(key, less, page, count);
            }
            catch (System.Data.DataException)
            {
                return vehicles;
            }

            if (vehiclesDB == null)
                return vehicles;

            foreach (var vehicle in vehiclesDB)
            {
                var current = TypeConterter.Convert(vehicle);

                vehicles.Add(current);
            }

            return vehicles;
        }

        public IEnumerable<IContract> SearchContracts(string key, double more, double less, int page, int count)
        {
            List<IContract> contracts = new List<IContract>();

            IEnumerable<DAL.Contracts.DTO.IContract> contractsDB = null;

            try
            {
                contractsDB = _contractRepository.SearchContracts(key, more, less, page, count);
            }
            catch (System.Data.DataException)
            {
                return contracts;
            }

            if (contractsDB == null)
                return contracts;

            foreach (var contract in contractsDB)
            {
                var current = TypeConterter.Convert(contract);

                contracts.Add(current);
            }

            return contracts;
        }

        public IEnumerable<ICategory> SearchCategories(string key,  int page, int count)
        {
            List<ICategory> categories = new List<ICategory>();

            IEnumerable<DAL.Contracts.DTO.ICategory> categoriesDB = null;

            try
            {
                categoriesDB = _categoryRepository.SearchCategories(key, page, count);
            }
            catch (System.Data.DataException)
            {
                return categories;
            }

            if (categoriesDB == null)
                return categories;

            foreach (var category in categoriesDB)
            {
                var current = TypeConterter.Convert(category);

                categories.Add(current);
            }

            return categories;
        }

        public int SaveNode(INode node)
        {
            var nodeMapped = new Node
            {
                Id = node.Id,
                CategoryId = node.CategoryId,
                ClientId = node.ClientId,
                ContractId = node.ContractId,
                VehicleId = node.VehicleId
            };

            if (nodeMapped.Id == 0)
            {
                return _nodeRepository.CreateNode(nodeMapped);
            }
            else
            {
                _nodeRepository.UpdateNode(nodeMapped);
                return nodeMapped.Id;
            }
        }

        public int SaveClient(IClient client)
        {
            var clientMapped = new Client
            {
                Id = client.Id,
                AddressId = client.AddressId,
                DriversExpirience = client.DriversExpirience,
                DriversLicenseNumber = client.DriversLicenseNumber,
                FirstName = client.FirstName,
                IsPhysicalPerson = client.IsPhysicalPerson,
                LastName = client.LastName,
                MiddleName = client.MiddleName,
                Passport = client.Passport,
                Phone = client.Phone,
                TaxNumber = client.TaxNumber
            };

            if (clientMapped.Id == 0)
            {
                return _clientRepository.CreateClient(clientMapped);
            }
            else
            {
                _clientRepository.UpdateClient(clientMapped);
                return clientMapped.Id;
            }
        }

        public int SaveVehicle(IVehicle vehicle)
        {
            var vehicleMapped = new Vehicle
            {
                Id = vehicle.Id,
                AmountOfSeats = vehicle.AmountOfSeats,
                Brand = vehicle.Brand,
                Carrying = vehicle.Carrying,
                CityOfRegistration = vehicle.CityOfRegistration,
                Color = vehicle.Color,
                LicensePlate = vehicle.LicensePlate,
                Number = vehicle.Number,
                Type = vehicle.Type,
                Value = vehicle.Value,
                Volume = vehicle.Volume,
                Year = vehicle.Year
            };

            if (vehicleMapped.Id == 0)
            {
                return _vehicleRepository.CreateVehicle(vehicleMapped);
            }
            else
            {
                _vehicleRepository.UpdateVehicle(vehicleMapped);
                return vehicleMapped.Id;
            }
        }

        public int SaveContract(IContract contract)
        {
            var contractMapped = new Contract
            {
                Id = contract.Id,
                BeginningDate = contract.BeginningDate,
                Brand = contract.Brand,
                ContractNumber = contract.ContractNumber,
                FinishingDate = contract.FinishingDate,
                Franchise = contract.Franchise,
                IsPhysicalPerson = contract.IsPhysicalPerson,
                VehicleOwnerName = contract.VehicleOwnerName,
                RegistrationDate = contract.RegistrationDate,
                Sticker = contract.Sticker,
                TaxNumber = contract.TaxNumber,
                Value = contract.Value
            };

            if (contractMapped.Id == 0)
            {
                return _contractRepository.CreateContract(contractMapped);
            }
            else
            {
                _contractRepository.UpdateContract(contractMapped);
                return contractMapped.Id;
            }
        }

        public int SaveCategory(ICategory category)
        {
            var categoryMapped = new Category
            {
                Id = category.Id,
                Description = category.Description,
                Name = category.Name
            };

            if (categoryMapped.Id == 0)
            {
                return _categoryRepository.CreateCategory(categoryMapped);
            }
            else
            {
                _categoryRepository.UpdateCategory(categoryMapped);
                return categoryMapped.Id;
            }
        }

        public int SaveAddress(IAddress address)
        {
            var addressMapped = new Address
            {
                Id = address.Id,
                Building = address.Building,
                City = address.City,
                Country = address.Country,
                FlatNumber = address.FlatNumber,
                PostCode = address.PostCode,
                Street = address.Street,
            };

            if (addressMapped.Id == 0)
            {
                return _addressRepository.CreateAddress(addressMapped);
            }
            else
            {
                _addressRepository.UpdateAddress(addressMapped);
                return addressMapped.Id;
            }
        }

        public void DeleteNode(int id)
        {
            _nodeRepository.DeleteNode(id);
        }

        public void DeleteClient(int id)
        {
            _clientRepository.DeleteClient(id);
        }

        public void DeleteVehicle(int id)
        {
            _vehicleRepository.DeleteVehicle(id);
        }

        public void DeleteContract(int id)
        {
            _contractRepository.DeleteContract(id);
        }

        public void DeleteCategory(int id)
        {
            _categoryRepository.DeleteCategory(id);
        }

        public void DeleteAddress(int id)
        {
            _addressRepository.DeleteAddress(id);
        }
    }
}
