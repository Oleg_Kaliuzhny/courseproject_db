﻿using BLL.Domains.DTO;
using DALDTO = DAL.Contracts.DTO;

namespace BLL.Domains
{
    static class TypeConterter
    {
        public static Node Convert(DALDTO.INode item)
        {
            if (item == null)
                return null;

            return new Node
            {
                Id = item.Id,
                CategoryId = item.CategoryId,
                ClientId = item.ClientId,
                ContractId = item.ContractId,
                VehicleId = item.VehicleId
            };
        }

        public static Client Convert(DALDTO.IClient item)
        {
            if (item == null)
                return null;

            return new DTO.Client
            {
                Id = item.Id,
                AddressId = item.AddressId,
                DriversExpirience = item.DriversExpirience,
                DriversLicenseNumber = item.DriversLicenseNumber,
                FirstName = item.FirstName,
                IsPhysicalPerson = item.IsPhysicalPerson,
                LastName = item.LastName,
                MiddleName = item.MiddleName,
                Passport = item.Passport,
                Phone = item.Phone,
                TaxNumber = item.TaxNumber,
            };
        }

        public static Contract Convert(DALDTO.IContract item)
        {
            if (item == null)
                return null;

            return new DTO.Contract
            {
                Id = item.Id,
                ContractNumber = item.ContractNumber,
                BeginningDate = item.BeginningDate,
                Brand = item.Brand,
                FinishingDate = item.FinishingDate,
                Franchise = item.Franchise,
                IsPhysicalPerson = item.IsPhysicalPerson,
                RegistrationDate = item.RegistrationDate,
                Sticker = item.Sticker,
                TaxNumber = item.TaxNumber,
                Value = item.Value,
                VehicleOwnerName = item.VehicleOwnerName
            };
        }

        public static Category Convert(DALDTO.ICategory item)
        {
            if (item == null)
                return null;

            return new DTO.Category
            {
                Id = item.Id,
                Description = item.Description,
                Name = item.Name,
            };
        }

        public static Vehicle Convert(DALDTO.IVehicle item)
        {
            if (item == null)
                return null;

            return new DTO.Vehicle
            {
                Id = item.Id,
                AmountOfSeats = item.AmountOfSeats,
                Brand = item.Brand,
                Carrying = item.Carrying,
                CityOfRegistration = item.CityOfRegistration,
                Color = item.Color,
                LicensePlate = item.LicensePlate,
                Number = item.Number,
                Type = item.Type,
                Value = item.Value,
                Volume = item.Volume,
                Year = item.Year
            };
        }

        public static Address Convert(DALDTO.IAddress item)
        {
            if (item == null)
                return null;

            return new DTO.Address
            {
                Building = item.Building,
                City = item.City,
                Country = item.Country,
                FlatNumber = item.FlatNumber,
                Id = item.Id,
                PostCode = item.PostCode,
                Street = item.Street
            };
        }
    }
}
