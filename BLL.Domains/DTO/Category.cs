﻿using BLL.Contracts.DTO;

namespace BLL.Domains.DTO
{
    public class Category : DTOBase, ICategory, DAL.Contracts.DTO.ICategory
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
