﻿using BLL.Contracts.DTO;
using System;

namespace BLL.Domains.DTO
{
    public class Client : DTOBase, IClient, DAL.Contracts.DTO.IClient, IComparable
    {
        public virtual string FirstName { get; set; }
        
        public virtual string LastName { get; set; }
        
        public virtual string MiddleName { get; set; }
        
        public virtual int TaxNumber { get; set; }
        
        public virtual bool IsPhysicalPerson { get; set; }
        
        public virtual string Passport { get; set; }
        
        public virtual int AddressId { get; set; }
        
        public virtual string Phone { get; set; }
        
        public virtual string DriversLicenseNumber { get; set; }

        public virtual int DriversExpirience { get; set; }

        public IAddress Address { get; set; }

        public string AddressValue { get => Address.ToString(); }

        public override string ToString()
        {
            return $"{LastName} {FirstName} {MiddleName}";
        }

        public int CompareTo(object obj)
        {
            return this.ToString().CompareTo((obj as Client).ToString());
        }
    }
}
