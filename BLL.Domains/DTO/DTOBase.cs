﻿using BLL.Contracts.DTO;

namespace BLL.Domains.DTO
{
    public abstract class DTOBase : IDTOBase
    {
        public virtual int Id { get; set; }
    }
}
