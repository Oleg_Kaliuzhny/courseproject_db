﻿using BLL.Contracts.DTO;

namespace BLL.Domains.DTO
{
    public class Address : DTOBase, IAddress, DAL.Contracts.DTO.IAddress
    {
        public virtual string Country { get; set; }
        
        public virtual string City { get; set; }
        
        public virtual int Building { get; set; }

        public virtual string Street { get; set; }

        public virtual int? FlatNumber { get; set; }
        
        public virtual int PostCode { get; set; }

        public override string ToString()
        {
            return $"{City}, {Street} {Building}, {PostCode}";
        }
    }
}
