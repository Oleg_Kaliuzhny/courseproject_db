﻿using BLL.Contracts.DTO;
using System;

namespace BLL.Domains.DTO
{
    public class Contract : DTOBase, IContract, DAL.Contracts.DTO.IContract
    {
        public virtual bool IsPhysicalPerson { get; set; }

        public virtual int TaxNumber { get; set; }

        public virtual string VehicleOwnerName { get; set; }

        public virtual string Brand { get; set; }

        public virtual double Value { get; set; }

        public virtual double Franchise { get; set; }

        public virtual DateTime BeginningDate { get; set; }

        public virtual DateTime FinishingDate { get; set; }

        public virtual DateTime RegistrationDate { get; set; }

        public virtual int Sticker { get; set; }

        public IVehicle Vehicle { get; set; }

        public IClient VehicleOwner { get; set; }

        public virtual int ContractNumber { get; set; }

        public override string ToString()
        {
            return ContractNumber.ToString();
        }
    }
}
