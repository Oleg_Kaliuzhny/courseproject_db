﻿using BLL.Contracts.DTO;

namespace BLL.Domains.DTO
{
    public class Node : DTOBase, INode, DAL.Contracts.DTO.INode
    {
        public virtual int ClientId { get; set; }

        public virtual int VehicleId { get; set; }

        public virtual int ContractId { get; set; }

        public virtual int CategoryId { get; set; }

        public ICategory Category { get; set; }

        public IClient Client { get; set; }

        public IContract Contract { get; set; }

        public IVehicle Vehicle { get; set; }

        public string CategoryValue => Category.ToString();

        public string ClientValue => Client.ToString();

        public string ContractValue => Contract.ToString();

        public string VehicleValue => Vehicle.ToString();
    }
}
