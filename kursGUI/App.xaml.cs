﻿using System;
using System.Windows;
using BLL.Contracts;
using IoC;

namespace kursGUI
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        View.MainWindow _mainWindow;
        ViewModel.MainWindowViewModel _mainWindowViewModel;
        public App()
        {
            INodes _dataList = new Container().GetNodesManager();

            _mainWindowViewModel = new ViewModel.MainWindowViewModel(_dataList);
            _mainWindow = new View.MainWindow();

            _mainWindowViewModel.CloseApplication += () => { if (this.IsNeedToCloseApp()) Quit(); };
            _mainWindow.Closing += (arg1, arg2) => { if (this.IsNeedToCloseApp()) Quit(); else arg2.Cancel = true; };

            _mainWindow.DataContext = _mainWindowViewModel;
            _mainWindow.Show();

        }

        bool IsNeedToCloseApp()
        {
            MessageBoxResult result = MessageBox.Show("Вы уверены?", "Выход", MessageBoxButton.YesNo);
            if (result.ToString() == "Yes")
                return true;
            return false;
        }

        void Quit()
        {
            Environment.Exit(0);
        }
    }
}
