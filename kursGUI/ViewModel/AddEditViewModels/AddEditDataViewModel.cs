﻿using BLL.Contracts.DTO;
using BLL.Domains.DTO;
using System;
using System.Collections.ObjectModel;
using System.Windows;

namespace kursGUI.ViewModel.AddEditViewModels
{
    class AddEditDataViewModel : AddEditViewModelBase<INode>
    {
        public AddEditDataViewModel()
        {

        }

        public ICategory SelectedCategory { get; set; }
        public IClient SelectedClient { get; set; }
        public IVehicle SelectedVehicle { get; set; }
        public IContract SelectedContract { get; set; }

        public ObservableCollection<ICategory> CategoriesCollection { get; set; }
        public ObservableCollection<IClient> ClientsCollection { get; set; }
        public ObservableCollection<IVehicle> VehiclesCollection { get; set; }
        public ObservableCollection<IContract> ContractsCollection { get; set; }

        protected override void Generate()
        {
            if (CheckValues())
            {
                GeneratedInstance = new Node
                {
                    Id = Id,
                    Category = SelectedCategory,
                    Client = SelectedClient,
                    Contract = SelectedContract,
                    Vehicle = SelectedVehicle,
                    CategoryId = SelectedCategory.Id,
                    ClientId = SelectedVehicle.Id,
                    ContractId = SelectedContract.Id,
                    VehicleId = SelectedVehicle.Id,
                };
                IsGenerated = true;
            }
        }

        private bool CheckValues()
        {
            if(SelectedCategory == null)
            {
                MessageBox.Show("Категория не может быть пустой.", "Упс!");
                return false;
            }

            if (SelectedClient == null)
            {
                MessageBox.Show("Клиент не может быть пустым.", "Упс!");
                return false;
            }

            if (SelectedContract == null)
            {
                MessageBox.Show("Контракт не может быть пустым.", "Упс!");
                return false;
            }

            if (SelectedVehicle == null)
            {
                MessageBox.Show("Транспорт не может быть пустым.", "Упс!");
                return false;
            }

            return true;
        }
    }
}
