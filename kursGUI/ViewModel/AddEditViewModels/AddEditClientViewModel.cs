﻿using BLL.Contracts.DTO;
using BLL.Domains.DTO;
using System;
using System.Windows;
using System.Windows.Media;

namespace kursGUI.ViewModel.AddEditViewModels
{
    class AddEditClientViewModel : AddEditViewModelBase<IClient>
    {
        public AddEditClientViewModel()
        {
            this.ClientCodeIDBrush = _gray;
            this.СlientDriverExpirienceBrush = _gray;
        }

        #region Client Info
        private string _clientName;

        public string ClientName
        {
            get { return _clientName; }
            set { _clientName = value; OnPropertyChanged("ClientName"); }
        }

        private string _clientLastName;

        public string ClientLastName
        {
            get { return _clientLastName; }
            set { _clientLastName = value; OnPropertyChanged("ClientLastName"); }
        }

        private string _clientPatronymic;

        public string ClientPatronymic
        {
            get { return _clientPatronymic; }
            set { _clientPatronymic = value; OnPropertyChanged("ClientPatronymic"); }
        }

        private string _clientCodeID;

        public string ClientCodeID
        {
            get { return _clientCodeID; }
            set { _clientCodeID = value; OnPropertyChanged("ClientCodeID"); }
        }

        private bool _clientFlag;

        public bool ClientFlag
        {
            get { return _clientFlag; }
            set { _clientFlag = value; OnPropertyChanged("ClientFlag"); }
        }

        private string _clientPassportNumber;

        public string ClientPassportNumber
        {
            get { return _clientPassportNumber; }
            set { _clientPassportNumber = value; OnPropertyChanged("ClientPassportNumber"); }
        }

        private string _clientAddressCountry;

        public string ClientAddressCountry
        {
            get { return _clientAddressCountry; }
            set { _clientAddressCountry = value; OnPropertyChanged("ClientAddressCountry"); }
        }

        private string _clientAddressCity;

        public string ClientAddressCity
        {
            get { return _clientAddressCity; }
            set { _clientAddressCity = value; OnPropertyChanged("ClientAddressCity"); }
        }

        private string _clientAddressStreet;

        public string ClientAddressStreet
        {
            get { return _clientAddressStreet; }
            set { _clientAddressStreet = value; OnPropertyChanged("ClientAddressStreet"); }
        }

        private string _clientAddressBuilding;

        public string ClientAddressBuilding
        {
            get { return _clientAddressBuilding; }
            set { _clientAddressBuilding = value; OnPropertyChanged("ClientAddressBuilding"); }
        }

        private string _clientAddressFlat;

        public string ClientAddressFlat
        {
            get { return _clientAddressFlat; }
            set { _clientAddressFlat = value; OnPropertyChanged("ClientAddressFlat"); }
        }

        private string _clientAddressPostCode;

        public string ClientAddressPostCode
        {
            get { return _clientAddressPostCode; }
            set { _clientAddressPostCode = value; OnPropertyChanged("ClientAddressPostCode"); }
        }

        private string _clientPhone;

        public string ClientPhone
        {
            get { return _clientPhone; }
            set { _clientPhone = value; OnPropertyChanged("ClientPhone"); }
        }

        private string _clientDriversLicense;

        public string ClientDriversLicense
        {
            get { return _clientDriversLicense; }
            set { _clientDriversLicense = value; OnPropertyChanged("ClientDriversLicense"); }
        }

        private string _clientDriversExpirience;

        public string ClientDriversExpirience
        {
            get { return _clientDriversExpirience; }
            set { _clientDriversExpirience = value; OnPropertyChanged("ClientDriversExpirience"); }
        }
        #endregion

        readonly SolidColorBrush _gray = new SolidColorBrush(Color.FromArgb(255, 171, 173, 179));

        #region Brushes
        private Brush _clientCodeIDBrush;

        public Brush ClientCodeIDBrush
        {
            get { return _clientCodeIDBrush; }
            set { _clientCodeIDBrush = value; OnPropertyChanged("ClientCodeIDBrush"); }
        }

        private Brush _clientDriverExpirienceBrush;

        public Brush СlientDriverExpirienceBrush
        {
            get { return _clientDriverExpirienceBrush; }
            set { _clientDriverExpirienceBrush = value; OnPropertyChanged("СlientDriverExpirienceBrush"); }
        }
        #endregion


        protected override void Generate()
        {
            //проверка ClientCodeID int
            try
            {
                Convert.ToInt32(this.ClientCodeID);
                ClientCodeIDBrush = _gray;
            }
            catch
            {
                ClientCodeIDBrush = Brushes.Red;
                IsGenerated = false;
                return;
            }

            //проверка ClientDriversExpirience int
            try
            {
                Convert.ToInt32(this.ClientDriversExpirience);
                СlientDriverExpirienceBrush = _gray;
            }
            catch
            {
                СlientDriverExpirienceBrush = Brushes.Red;
                IsGenerated = false;
                return;
            }

            if (string.IsNullOrWhiteSpace(ClientLastName) ||
                string.IsNullOrWhiteSpace(ClientName) ||
                string.IsNullOrWhiteSpace(ClientPatronymic) ||
                string.IsNullOrWhiteSpace(ClientCodeID) ||
                string.IsNullOrWhiteSpace(ClientPassportNumber) ||
                string.IsNullOrWhiteSpace(ClientPhone) ||
                string.IsNullOrWhiteSpace(ClientDriversLicense) ||
                string.IsNullOrWhiteSpace(ClientDriversExpirience) ||
                string.IsNullOrWhiteSpace(ClientAddressCountry) ||
                string.IsNullOrWhiteSpace(ClientAddressCity) ||
                string.IsNullOrWhiteSpace(ClientAddressStreet))
            {
                IsGenerated = false;
                MessageBox.Show("Поля не должны быть пустымы или состоять только из пробелов.");
                return;
            }

            GeneratedInstance = new Client
            {
                Id = Id,
                LastName = ClientLastName,
                FirstName = ClientName,
                MiddleName = ClientPatronymic,
                TaxNumber = int.Parse(ClientCodeID),
                Passport = ClientPassportNumber,
                Phone = ClientPhone,
                DriversLicenseNumber = ClientDriversLicense,
                DriversExpirience = int.Parse(ClientDriversExpirience),
                IsPhysicalPerson = ClientFlag,
                Address = new Address
                {
                    Country = ClientAddressCountry,
                    City = ClientAddressCity,
                    Street = ClientAddressStreet,
                    Building = int.Parse(ClientAddressBuilding),
                    FlatNumber = int.Parse(ClientAddressFlat),
                    PostCode = int.Parse(ClientAddressPostCode)
                }
            };
            IsGenerated = true;
        }
    }
}
