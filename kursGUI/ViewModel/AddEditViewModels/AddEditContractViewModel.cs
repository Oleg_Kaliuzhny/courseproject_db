﻿using BLL.Contracts.DTO;
using BLL.Domains.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace kursGUI.ViewModel.AddEditViewModels
{
    public class AddEditContractViewModel : AddEditViewModelBase<IContract>
    {
        public AddEditContractViewModel()
        {
            this.ContractCodeIDBrush = _gray;
            this.ContractFranchiseBrush = _gray;
            this.ContractNumberBrush = _gray;
            this.ContractStickerBrush = _gray;
            this.ContractValueBrush = _gray;
            this.ContractBeginningDate = DateTime.Now;
            this.ContractFinishingDate = DateTime.Now;
            this.ContractRegistrationDate = DateTime.Now;
        }

        #region Fields & Properties
        readonly SolidColorBrush _gray = new SolidColorBrush(Color.FromArgb(255, 171, 173, 179));
        #endregion

        #region Brushes
        private Brush _contractNumberBrush;

        public Brush ContractNumberBrush
        {
            get { return _contractNumberBrush; }
            set { _contractNumberBrush = value; OnPropertyChanged("ContractNumberBrush"); }
        }

        private Brush _contractCodeIDBrush;

        public Brush ContractCodeIDBrush
        {
            get { return _contractCodeIDBrush; }
            set { _contractCodeIDBrush = value; OnPropertyChanged("ContractCodeIDBrush"); }
        }

        private Brush _contractValueBrush;

        public Brush ContractValueBrush
        {
            get { return _contractValueBrush; }
            set { _contractValueBrush = value; OnPropertyChanged("ContractValueBrush"); }
        }

        private Brush _cotractFranchiseBrush;

        public Brush ContractFranchiseBrush
        {
            get { return _cotractFranchiseBrush; }
            set { _cotractFranchiseBrush = value; OnPropertyChanged("ContractFranchiseBrush"); }
        }

        private Brush _contractStickerBrush;

        public Brush ContractStickerBrush
        {
            get { return _contractStickerBrush; }
            set { _contractStickerBrush = value; OnPropertyChanged("ContractStickerBrush"); }
        }


        #endregion

        #region Contract Info
        private string _contractNumber;

        public string ContractNumber
        {
            get { return _contractNumber; }
            set { _contractNumber = value; OnPropertyChanged("ContractNumber"); }
        }

        private string _contractCodeID;

        public string ContractCodeID
        {
            get { return _contractCodeID; }
            set { _contractCodeID = value; OnPropertyChanged("ContractCodeID"); }
        }

        private bool _contractFlag;

        public bool ContractFlag
        {
            get { return _contractFlag; }
            set { _contractFlag = value; OnPropertyChanged("ContractFlag"); }
        }

        private string _contractBrand;

        public string ContractBrand
        {
            get { return _contractBrand; }
            set { _contractBrand = value; OnPropertyChanged("ContractBrand"); }
        }

        private string _contractValue;

        public string ContractValue
        {
            get { return _contractValue; }
            set { _contractValue = value; OnPropertyChanged("ContractValue"); }
        }

        private string _contractOwnerName;

        public string ContractOwnerName
        {
            get { return _contractOwnerName; }
            set { _contractOwnerName = value; OnPropertyChanged("ContractOwnerName"); }
        }

        private string _contractFranchise;

        public string ContractFranchise
        {
            get { return _contractFranchise; }
            set { _contractFranchise = value; OnPropertyChanged("ContractFranchise"); }
        }

        private DateTime _contractBeginningDate;

        public DateTime ContractBeginningDate
        {
            get { return _contractBeginningDate; }
            set { _contractBeginningDate = value; OnPropertyChanged("ContractBeginningDate"); }
        }

        private DateTime _contractFinishingDate;

        public DateTime ContractFinishingDate
        {
            get { return _contractFinishingDate; }
            set { _contractFinishingDate = value; OnPropertyChanged("ContractFinishingDate"); }
        }

        private DateTime _contractRegistrationDate;

        public DateTime ContractRegistrationDate
        {
            get { return _contractRegistrationDate; }
            set { _contractRegistrationDate = value; OnPropertyChanged("ContractRegistrationDate"); }
        }

        private string _contractSticker;

        public string ContractSticker
        {
            get { return _contractSticker; }
            set { _contractSticker = value; OnPropertyChanged("ContractSticker"); }
        }

        #endregion


        bool CheckValues()
        {
            bool isOk = true;

            //ContractNumber double
            try
            {
                Convert.ToInt32(this.ContractNumber);
                ContractNumberBrush = _gray;
            }
            catch
            {
                ContractNumberBrush = Brushes.Red;
                isOk = false;
            }

            //ContractCodeID int
            try
            {
                Convert.ToUInt32(this.ContractCodeID);
                ContractCodeIDBrush = _gray;
            }
            catch
            {
                ContractCodeIDBrush = Brushes.Red;
                isOk = false;
            }

            //ContractValue double
            try
            {
                Convert.ToDouble(this.ContractValue);
                ContractValueBrush = _gray;
            }
            catch
            {
                ContractValueBrush = Brushes.Red;
                isOk = false;
            }

            //ContractFranchise double
            try
            {
                Convert.ToDouble(this.ContractFranchise);
                ContractFranchiseBrush = _gray;
            }
            catch
            {
                ContractFranchiseBrush = Brushes.Red;
                isOk = false;
            }

            //ContractSticker int
            try
            {
                Convert.ToUInt32(this.ContractSticker);
                ContractStickerBrush = _gray;
            }
            catch
            {
                ContractStickerBrush = Brushes.Red;
                isOk = false;
            }

            if (string.IsNullOrWhiteSpace(ContractNumber) ||
                string.IsNullOrWhiteSpace(ContractCodeID) ||
                string.IsNullOrWhiteSpace(ContractBrand) ||
                string.IsNullOrWhiteSpace(ContractValue) ||
                string.IsNullOrWhiteSpace(ContractOwnerName) ||
                string.IsNullOrWhiteSpace(ContractFranchise) ||
                string.IsNullOrWhiteSpace(ContractSticker))
            {
                isOk = false;
                MessageBox.Show("Строки не должны быть пустыми или состоять только из пробелов!", "Упс!");
            }

            if (ContractBeginningDate < new DateTime(1980,1,1) ||
                ContractFinishingDate < new DateTime(1980, 1, 1) ||
                ContractRegistrationDate < new DateTime(1980, 1, 1))
            {
                isOk = false;
                MessageBox.Show("Дата не может быть меньше 01.01.1980!", "Упс!");
            }

            return isOk;
        }

        protected override void Generate()
        {
            if (CheckValues())
            {
                GeneratedInstance = new Contract
                {
                    Id = Id,
                    BeginningDate = ContractBeginningDate,
                    Brand = ContractBrand,
                    FinishingDate = ContractFinishingDate,
                    Franchise = Convert.ToDouble(ContractFranchise),
                    IsPhysicalPerson = ContractFlag,
                    RegistrationDate = ContractRegistrationDate,
                    Sticker = Convert.ToInt32(ContractSticker),
                    TaxNumber = Convert.ToInt32(ContractCodeID),
                    Value = Convert.ToDouble(ContractValue),
                    VehicleOwnerName = ContractOwnerName,
                    ContractNumber = Convert.ToInt32(ContractNumber),
                };
                IsGenerated = true;
            }
        }
    }
}
