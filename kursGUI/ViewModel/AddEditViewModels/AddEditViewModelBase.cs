﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace kursGUI.ViewModel.AddEditViewModels
{
    public abstract class AddEditViewModelBase<TGenerated> : ViewModelBase
    {
        public AddEditViewModelBase()
        {
            OkCommand = new Command(arg => Generate());
        }

        private bool _isGenerated;

        public ICommand OkCommand { get; set; }

        public int Id { get; set; }

        public bool IsGenerated
        {
            get
            {
                return _isGenerated;
            }
            protected set
            {
                _isGenerated = value;
                OnPropertyChanged("IsGenerated");
            }
        }

        public TGenerated GeneratedInstance { get; protected set; }

        protected abstract void Generate();
    }
}
