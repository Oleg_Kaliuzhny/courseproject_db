﻿using BLL.Contracts.DTO;
using BLL.Domains.DTO;
using System.Windows;

namespace kursGUI.ViewModel.AddEditViewModels
{
    public class AddEditCategoryViewModel : AddEditViewModelBase<ICategory>
    {
        private string _title;

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        private string _description;

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        protected override void Generate()
        {
            if (string.IsNullOrWhiteSpace(_title))
            {
                IsGenerated = false;
                MessageBox.Show("Заголовок не может быть пустым или состоять только из пробелов.");
                return;
            }

            GeneratedInstance = new Category
            {
                Id = Id,
                Name = _title,
                Description = _description
            };
            IsGenerated = true;
        }
    }
}
