﻿using BLL.Contracts.DTO;
using BLL.Domains.DTO;
using System;
using System.Windows;
using System.Windows.Media;

namespace kursGUI.ViewModel.AddEditViewModels
{
    public class AddEditVehicleViewModel : AddEditViewModelBase<IVehicle>
    {
        public AddEditVehicleViewModel()
        {
            this.VehicleAmountOfSeatsBrush = _gray;
            this.VehicleCarryingBrush = _gray;
            this.VehicleNumberBrush = _gray;
            this.VehicleValueBrush = _gray;
            this.VehicleVolumeBrush = _gray;
            this.VehicleYearBrush = _gray;
        }

        #region Brushes
        private Brush _vehicleNumberBrush;

        public Brush VehicleNumberBrush
        {
            get { return _vehicleNumberBrush; }
            set { _vehicleNumberBrush = value; OnPropertyChanged("VehicleNumberBrush"); }
        }

        private Brush _vehicleYearBrush;

        public Brush VehicleYearBrush
        {
            get { return _vehicleYearBrush; }
            set { _vehicleYearBrush = value; OnPropertyChanged("VehicleYearBrush"); }
        }

        private Brush _vehicleValueBrush;

        public Brush VehicleValueBrush
        {
            get { return _vehicleValueBrush; }
            set { _vehicleValueBrush = value; OnPropertyChanged("VehicleValueBrush"); }
        }

        private Brush _vehicleVolumeBrush;

        public Brush VehicleVolumeBrush
        {
            get { return _vehicleVolumeBrush; }
            set { _vehicleVolumeBrush = value; OnPropertyChanged("VehicleVolumeBrush"); }
        }

        private Brush _vehicleAmountOfSeatsBrush;

        public Brush VehicleAmountOfSeatsBrush
        {
            get { return _vehicleAmountOfSeatsBrush; }
            set { _vehicleAmountOfSeatsBrush = value; OnPropertyChanged("VehicleAmountOfSeatsBrush"); }
        }

        private Brush _vehicleCarryingBrush;

        public Brush VehicleCarryingBrush
        {
            get { return _vehicleCarryingBrush; }
            set { _vehicleCarryingBrush = value; OnPropertyChanged("VehicleCarryingBrush"); }
        }
        #endregion

        #region Vehicle Info
        private string _vehicleBrand;

        public string VehicleBrand
        {
            get { return _vehicleBrand; }
            set { _vehicleBrand = value; OnPropertyChanged("VehicleBrand"); }
        }

        private string _vehicleNumber;

        public string VehicleNumber
        {
            get { return _vehicleNumber; }
            set { _vehicleNumber = value; OnPropertyChanged("VehicleNumber"); }
        }

        private string _vehicleYear;

        public string VehicleYear
        {
            get { return _vehicleYear; }
            set { _vehicleYear = value; OnPropertyChanged("VehicleYear"); }
        }

        private string _vehicleLicensePlate;

        public string VehicleLicense
        {
            get { return _vehicleLicensePlate; }
            set { _vehicleLicensePlate = value; OnPropertyChanged("VehicleLicense"); }
        }

        private string _vehicleValue;

        public string VehicleValue
        {
            get { return _vehicleValue; }
            set { _vehicleValue = value; OnPropertyChanged("VehicleValue"); }
        }

        private string _vehicleColor;

        public string VehicleColor
        {
            get { return _vehicleColor; }
            set { _vehicleColor = value; OnPropertyChanged("VehicleColor"); }
        }

        private string _vehicleVolume;

        public string VehicleVolume
        {
            get { return _vehicleVolume; }
            set { _vehicleVolume = value; OnPropertyChanged("VehicleVolume"); }
        }

        private string _vehicleAmountOfSeats;

        public string VehicleAmountOfSeats
        {
            get { return _vehicleAmountOfSeats; }
            set { _vehicleAmountOfSeats = value; OnPropertyChanged("VehicleAmountOfSeats"); }
        }

        private string _vehicleCarrying;

        public string VehicleCarrying
        {
            get { return _vehicleCarrying; }
            set { _vehicleCarrying = value; OnPropertyChanged("VehicleCarrying"); }
        }

        private string _vehicleCityOfRegistration;

        public string VehicleCityOfRegistration
        {
            get { return _vehicleCityOfRegistration; }
            set { _vehicleCityOfRegistration = value; OnPropertyChanged("VehicleCityOfRegistration"); }
        }

        private string _vehicleType;

        public string VehicleType
        {
            get { return _vehicleType; }
            set { _vehicleType = value; OnPropertyChanged("VehicleType"); }
        }

        #endregion

        readonly SolidColorBrush _gray = new SolidColorBrush(Color.FromArgb(255, 171, 173, 179));

        bool CheckValues()
        {
            bool isOk = true;
            //проверка VehicleNumber int
            try
            {
                Convert.ToUInt32(this.VehicleNumber);
                VehicleNumberBrush = _gray;
            }
            catch
            {
                VehicleNumberBrush = Brushes.Red;
                isOk = false;
            }

            //VehicleYear int
            try
            {
                Convert.ToUInt32(this.VehicleYear);
                VehicleYearBrush = _gray;
            }
            catch
            {
                VehicleYearBrush = Brushes.Red;
                isOk = false;
            }

            //VehicleValue double
            try
            {
                Convert.ToDouble(this.VehicleValue);
                VehicleValueBrush = _gray;
            }
            catch
            {
                VehicleValueBrush = Brushes.Red;
                isOk = false;
            }

            //VehicleVolume double
            try
            {
                Convert.ToDouble(this.VehicleVolume);
                VehicleVolumeBrush = _gray;
            }
            catch
            {
                VehicleVolumeBrush = Brushes.Red;
                isOk = false;
            }

            //VehicleAmountOfSeats int
            try
            {
                Convert.ToUInt32(this.VehicleAmountOfSeats);
                VehicleAmountOfSeatsBrush = _gray;
            }
            catch
            {
                VehicleAmountOfSeatsBrush = Brushes.Red;
                isOk = false;
            }

            //VehicleCarrying double
            try
            {
                Convert.ToDouble(this.VehicleCarrying);
                VehicleCarryingBrush = _gray;
            }
            catch
            {
                VehicleCarryingBrush = Brushes.Red;
                isOk = false;
            }

            if (string.IsNullOrWhiteSpace(VehicleBrand) ||
                string.IsNullOrWhiteSpace(VehicleNumber) ||
                string.IsNullOrWhiteSpace(VehicleYear) ||
                string.IsNullOrWhiteSpace(VehicleLicense) ||
                string.IsNullOrWhiteSpace(VehicleValue) ||
                string.IsNullOrWhiteSpace(VehicleColor) ||
                string.IsNullOrWhiteSpace(VehicleVolume) ||
                string.IsNullOrWhiteSpace(VehicleAmountOfSeats) ||
                string.IsNullOrWhiteSpace(VehicleCarrying) ||
                string.IsNullOrWhiteSpace(VehicleCityOfRegistration) ||
                string.IsNullOrWhiteSpace(VehicleType))
            {
                isOk = false;
                MessageBox.Show("Строки не должны быть пустыми или состоять только из пробелов!", "Упс!");
            }

            return isOk;
        }

        protected override void Generate()
        {
            if (CheckValues())
            {
                GeneratedInstance = new Vehicle
                {
                    Id = Id,
                    AmountOfSeats = Convert.ToInt32(VehicleAmountOfSeats),
                    Brand = VehicleBrand,
                    Carrying = Convert.ToInt32(VehicleCarrying),
                    CityOfRegistration = VehicleCityOfRegistration,
                    Color = VehicleColor,
                    LicensePlate = VehicleLicense,
                    Number = Convert.ToInt32(VehicleNumber),
                    Type = VehicleType,
                    Value = Convert.ToDouble(VehicleValue),
                    Volume = Convert.ToDouble(VehicleVolume),
                    Year = Convert.ToInt32(VehicleYear)
                };
                IsGenerated = true;
            }
        }
    }
}
