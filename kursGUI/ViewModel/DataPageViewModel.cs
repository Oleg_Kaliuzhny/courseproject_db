﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using kursGUI.View;
using System;
using BLL.Contracts;
using BLL.Contracts.DTO;
using System.Threading.Tasks;

namespace kursGUI.ViewModel
{
    public class DataPageViewModel : ViewModelBase
    {
        #region Constructor
        public DataPageViewModel(INodes nodes)
        {
            _nodes = nodes;
            Collection = new ObservableCollection<INode>();
            this.FormVehiclesCollection(nodes);

            this.BeginSearchCommand = new Command(arg => Search());
            this.UpdateCommand = new Command(arg => FormVehiclesCollection(nodes));
            this.AddCommand = new Command(arg => Add());
            this.EditCommand = new Command(arg =>
            {
                if (SelectedItem != null)
                    Add(SelectedItem);
            });
            Collection.CollectionChanged += CollectionCollectionChanged;
        }
        #endregion

        #region Fields
        INodes _nodes;
        #endregion

        #region Properties
        #region Search
        private string _searchQuery;

        public string SearchQuery
        {
            get { return _searchQuery; }
            set { _searchQuery = value; this.Search(); }
        }

        private string _searchValueFrom;

        public string SearchValueFrom
        {
            get { return _searchValueFrom; }
            set { _searchValueFrom = value; /* this.Search(); */}
        }

        private string _searchValueTo;

        public string SearchValueTo
        {
            get { return _searchValueTo; }
            set { _searchValueTo = value; /* this.Search(); */}
        }

        private bool _searchValueLessThen300;

        public bool SearchValueLessThen300
        {
            get { return _searchValueLessThen300; }
            set { _searchValueLessThen300 = value; /* this.Search(); */}
        }

        #endregion

        public INode SelectedItem { get; set; }

        private ObservableCollection<INode> _collection;

        public ObservableCollection<INode> Collection
        {
            get { return _collection; }
            set { _collection = value; OnPropertyChanged("Collection"); }
        }

        public ICommand BeginSearchCommand { get; set; }
        public ICommand UpdateCommand { get; set; }
        public ICommand AddCommand { get; set; }
        public ICommand EditCommand { get; set; }

        #endregion

        #region Methods
        public void FormVehiclesCollection(INodes nodes)
        {
            Collection.Clear();
            
            foreach (var item in nodes.GetNodes(0, int.MaxValue))
            {
                Collection.Add(item);
            }
        }

        async void Search()
        {
            Collection.Clear();

            var items = await Task.Run(() => _nodes.SearchNodes(SearchQuery ?? "", 0, int.MaxValue));

            foreach (var item in items)
            {
                Collection.Add(item);
            }
        }

        private void CollectionCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove)
            {
                foreach (var item in e.OldItems)
                {
                    _nodes.DeleteNode((item as INode).Id);
                }
            }
        }

        private void Add(INode node = null)
        {
            var addViewModel = new AddEditViewModels.AddEditDataViewModel();

            addViewModel.CategoriesCollection = new ObservableCollection<ICategory>(_nodes.GetCategories(0, int.MaxValue));
            addViewModel.ClientsCollection = new ObservableCollection<IClient>(_nodes.GetClients(0, int.MaxValue));
            addViewModel.ContractsCollection = new ObservableCollection<IContract>(_nodes.GetContracts(0, int.MaxValue));
            addViewModel.VehiclesCollection = new ObservableCollection<IVehicle>(_nodes.GetVehicles(0, int.MaxValue));

            if (node != null)
            {
                addViewModel.Id = node.Id;
                addViewModel.SelectedCategory = node.Category;
                addViewModel.SelectedClient = node.Client;
                addViewModel.SelectedContract = node.Contract;
                addViewModel.SelectedVehicle = node.Vehicle;
            }

            var addView = new AddData();
            addView.DataContext = addViewModel;
            addView.ShowDialog();

            if (addViewModel.IsGenerated)
            {
                _nodes.SaveNode(addViewModel.GeneratedInstance);
                FormVehiclesCollection(_nodes);
            }
        }
        #endregion
    }
}
