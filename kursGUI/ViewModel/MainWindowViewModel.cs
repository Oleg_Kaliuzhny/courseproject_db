﻿using kursGUI.View.Pages;
using System;
using System.Windows;
using System.Windows.Input;
using IoC;
using BLL.Contracts;

namespace kursGUI.ViewModel
{
    public class MainWindowViewModel : ViewModelBase
    {
        #region Contructor
        public MainWindowViewModel(INodes nodes)
        {
            _nodes = nodes;
            this.Page = _viewMenu; //стартовая страница - меню
            _viewMenu.DataContext = _menuViewModel;

            _dataPageViewModel = new DataPageViewModel(nodes);
            _viewData.DataContext = _dataPageViewModel;

            _clientsPageViewModel = new ClientsPageViewModel(nodes);
            _viewClients.DataContext = _clientsPageViewModel;

            _vehiclesPageViewModel = new VehiclesPageViewModel(nodes);
            _viewVehicles.DataContext = _vehiclesPageViewModel;

            _contractsPageViewModel = new ContractsPageViewModel(nodes);
            _viewContracts.DataContext = _contractsPageViewModel;

            _categoriesViewModel = new CategoriesViewModel(_nodes);
            _viewCategories.DataContext = _categoriesViewModel;

            //_graphPageViewModel = new GraphPageViewModel(_dataList);
            //_viewGraph.DataContext = _graphPageViewModel;

            _menuViewModel.PageSelected += ChangePage;

            CreateCommand = new Command(args => this.Create());
            SaveCommand = new Command(args => this.Save());
            OpenCommand = new Command(args => this.Open());
            MenuCommand = new Command(args => this.Menu());
            ExitCommand = new Command(args => this.Exit());
        }
        #endregion

        #region Fields
        INodes _nodes;

        MenuPageViewModel _menuViewModel = new MenuPageViewModel();
        DataPageViewModel _dataPageViewModel;
        ClientsPageViewModel _clientsPageViewModel;
        VehiclesPageViewModel _vehiclesPageViewModel;
        ContractsPageViewModel _contractsPageViewModel;
        CategoriesViewModel _categoriesViewModel;
        GraphPageViewModel _graphPageViewModel;

        ViewMenuPage _viewMenu = new ViewMenuPage();
        ViewDataPage _viewData = new ViewDataPage();
        ViewClientsPage _viewClients = new ViewClientsPage();
        ViewVehiclesPage _viewVehicles = new ViewVehiclesPage();
        ViewContractsPage _viewContracts = new ViewContractsPage();
        ViewCategoriesPage _viewCategories = new ViewCategoriesPage();
        ViewGraphPage _viewGraph = new ViewGraphPage();
        #endregion

        #region Properties
        private string _status;

        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                MessageBox.Show(value);
                OnPropertyChanged("Status");
            }
        }

        private object _page;

        public object Page
        {
            get
            {
                return _page;
            }
            set
            {
                _page = value;
                OnPropertyChanged("Page");
            }
        }

        public ICommand CreateCommand { get; set; }
        public ICommand SaveCommand { get; set; }
        public ICommand OpenCommand { get; set; }
        public ICommand MenuCommand { get; set; }
        public ICommand ExitCommand { get; set; }
        #endregion

        #region Events
        public event Action CloseApplication;
        #endregion

        #region Methods
        void Create()
        {
            
        }

        void Save()
        {
            
        }

        void Open()
        {
            
        }

        void Menu()
        {
            this.Page = _viewMenu;
        }

        void Exit()
        {
            this.CloseApplication?.Invoke();
        }

        void ChangePage(string page)
        {
            switch (page)
            {
                case "DataPage":
                    Page = _viewData;
                    break;
                case "ClientsPage":
                    Page = _viewClients;
                    break;
                case "VehiclesPage":
                    Page = _viewVehicles;
                    break;
                case "ContractsPage":
                    Page = _viewContracts;
                    break;
                case "CategoriesPage":
                    Page = _viewCategories;
                    break;
                case "GraphPage":
                    Page = _viewGraph;
                    break;
                default:
                    break;
            }
        }
        #endregion
    }
}
