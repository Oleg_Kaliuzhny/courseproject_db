﻿using BLL.Contracts;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace kursGUI.ViewModel
{
    public class GraphPageViewModel : ViewModelBase
    {
        #region Constructor
        public GraphPageViewModel(INodes nodes)
        {
            _dataList = nodes ?? throw new ArgumentNullException(nameof(nodes));
            Statistics = new ObservableCollection<StatisticData>();
            SelectionMenu = new List<string>();
            GenerateSelectionMenu();
            SelectedIndex = 0;
        }
        #endregion

        #region Properties & Fields
        INodes _dataList;
        public List<string> SelectionMenu { get; private set; }

        private ObservableCollection<StatisticData> _statistics;

        public ObservableCollection<StatisticData> Statistics
        {
            get
            {
                return _statistics;
            }
            private set
            {
                _statistics = value;
                OnPropertyChanged("Statistics");
            }
        }

        public string Title
        {
            get
            {
                return "Количество новых клиентов за " + TitleEnd(SelectedIndex);
            }
        }

        private object _selectedItem;

        public object SelectedItem
        {
            get
            {
                return _selectedItem;
            }
            set
            {
                _selectedItem = value;
                OnPropertyChanged("SelectedItem");
            }
        }

        private int _selectedIndex;

        public int SelectedIndex
        {
            get { return _selectedIndex; }
            set
            {
                _selectedIndex = value;
                OnPropertyChanged("SelectedIndex");
                OnPropertyChanged("Title");
                GenerateStatistics(_dataList, value);
            }
        }

        #endregion

        #region Methods
        public void GenerateStatistics(INodes dataList, int index = 0)
        {
            //if (index == 0)
            //{
            //    Statistics.Clear();
            //    return;
            //}

            //DateTime time = DateTime.Now;
            //TimeSpan ts = time.Subtract(new DateTime(1, index, 1));
            //time = new DateTime(ts.Ticks);

            //Statistics.Clear();

            ////получение записей конкретного месяца
            //List<IData> result = new List<IData>();
            //foreach (var data in dataList.ListOfData)
            //{
            //    if (data.Contract.RegistrationDate.Month == time.Month && data.Contract.RegistrationDate.Year == time.Year)
            //        result.Add(data);
            //}

            ////разбитие на интервалы 
            //int days = DateTime.DaysInMonth(time.Year, time.Month);
            //for (int i = 1; i <= days; i++)
            //{
            //    StatisticData statData = new StatisticData();
            //    statData.Date = i + "/" + time.Month + "/" + time.Year;
            //    int counter = 0;
            //    foreach (var data in result)
            //    {
            //        if (data.Contract.RegistrationDate.Day == i)
            //            counter++;
            //    }
            //    statData.Amount = counter;
            //    Statistics.Add(statData);
            //}
            //OnPropertyChanged("Statistics");
        }

        void GenerateSelectionMenu()
        {
            SelectionMenu.Add("Выбор месяца");
            SelectionMenu.Add(DateTime.Today.Month.ToString() + "." + DateTime.Today.Year.ToString());
            for (int i = 0; i < 3; i++)
            {
                DateTime time = DateTime.Now;
                TimeSpan ts = time.Subtract(new DateTime(1, i + 2, 1));
                time = new DateTime(ts.Ticks);
                SelectionMenu.Add(time.Month.ToString() + "." + time.Year.ToString());
            }
        }

        string TitleEnd(int index)
        {
            if (index == 0)
                return "???";
            DateTime time = DateTime.Now;
            TimeSpan ts = time.Subtract(new DateTime(1, index, 1));
            time = new DateTime(ts.Ticks);
            string end = "";
            switch (time.Month)
            {
                case 1:
                    end += "январь";
                    break;
                case 2:
                    end += "февраль";
                    break;
                case 3:
                    end += "март";
                    break;
                case 4:
                    end += "апрель";
                    break;
                case 5:
                    end += "май";
                    break;
                case 6:
                    end += "июнь";
                    break;
                case 7:
                    end += "июль";
                    break;
                case 8:
                    end += "август";
                    break;
                case 9:
                    end += "сентябрь";
                    break;
                case 10:
                    end += "октябрь";
                    break;
                case 11:
                    end += "ноябрь";
                    break;
                case 12:
                    end += "декабрь";
                    break;
                default:
                    end += "";
                    break;
            }
            end += " " + time.Year.ToString() + " года";
            return end;
        }
        #endregion
        public class StatisticData
        {
            public string Date { get; set; }

            public int Amount { get; set; }
        }
    }
}
