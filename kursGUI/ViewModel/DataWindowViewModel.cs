﻿using System;
using System.Windows.Input;
using System.Windows;
using System.Windows.Media;
using BLL.Contracts;
using BLL.Contracts.DTO;

namespace kursGUI.ViewModel
{
    public class DataWindowViewModel : ViewModelBase
    {
        #region Contructors
        public DataWindowViewModel()
        {
            OKCommand = new Command(arg => GenerateData());
            this.ClientCodeIDBrush = _gray;
            this.СlientDriverExpirienceBrush = _gray;
            this.ContractCodeIDBrush = _gray;
            this.ContractFranchiseBrush = _gray;
            this.ContractNumberBrush = _gray;
            this.ContractStickerBrush = _gray;
            this.ContractValueBrush = _gray;
            this.VehicleAmountOfSeatsBrush = _gray;
            this.VehicleCarryingBrush = _gray;
            this.VehicleNumberBrush = _gray;
            this.VehicleValueBrush = _gray;
            this.VehicleVolumeBrush = _gray;
            this.VehicleYearBrush = _gray;
        }

        public DataWindowViewModel(INode data) : this()
        {
            this.DataForChanging = data;
        }
        #endregion

        #region Fields & Properties
        readonly SolidColorBrush _gray = new SolidColorBrush(Color.FromArgb(255, 171, 173, 179));

        #region Brushes
        private Brush _clientCodeIDBrush;

        public Brush ClientCodeIDBrush
        {
            get { return _clientCodeIDBrush; }
            set { _clientCodeIDBrush = value; OnPropertyChanged("ClientCodeIDBrush"); }
        }

        private Brush _clientDriverExpirienceBrush;

        public Brush СlientDriverExpirienceBrush
        {
            get { return _clientDriverExpirienceBrush; }
            set { _clientDriverExpirienceBrush = value; OnPropertyChanged("СlientDriverExpirienceBrush"); }
        }

        private Brush _vehicleNumberBrush;

        public Brush VehicleNumberBrush
        {
            get { return _vehicleNumberBrush; }
            set { _vehicleNumberBrush = value; OnPropertyChanged("VehicleNumberBrush"); }
        }

        private Brush _vehicleYearBrush;

        public Brush VehicleYearBrush
        {
            get { return _vehicleYearBrush; }
            set { _vehicleYearBrush = value; OnPropertyChanged("VehicleYearBrush"); }
        }

        private Brush _vehicleValueBrush;

        public Brush VehicleValueBrush
        {
            get { return _vehicleValueBrush; }
            set { _vehicleValueBrush = value; OnPropertyChanged("VehicleValueBrush"); }
        }

        private Brush _vehicleVolumeBrush;

        public Brush VehicleVolumeBrush
        {
            get { return _vehicleVolumeBrush; }
            set { _vehicleVolumeBrush = value; OnPropertyChanged("VehicleVolumeBrush"); }
        }

        private Brush _vehicleAmountOfSeatsBrush;

        public Brush VehicleAmountOfSeatsBrush
        {
            get { return _vehicleAmountOfSeatsBrush; }
            set { _vehicleAmountOfSeatsBrush = value; OnPropertyChanged("VehicleAmountOfSeatsBrush"); }
        }

        private Brush _vehicleCarryingBrush;

        public Brush VehicleCarryingBrush
        {
            get { return _vehicleCarryingBrush; }
            set { _vehicleCarryingBrush = value; OnPropertyChanged("VehicleCarryingBrush"); }
        }

        private Brush _contractNumberBrush;

        public Brush ContractNumberBrush
        {
            get { return _contractNumberBrush; }
            set { _contractNumberBrush = value; OnPropertyChanged("ContractNumberBrush"); }
        }

        private Brush _contractCodeIDBrush;

        public Brush ContractCodeIDBrush
        {
            get { return _contractCodeIDBrush; }
            set { _contractCodeIDBrush = value; OnPropertyChanged("ContractCodeIDBrush"); }
        }

        private Brush _contractValueBrush;

        public Brush ContractValueBrush
        {
            get { return _contractValueBrush; }
            set { _contractValueBrush = value; OnPropertyChanged("ContractValueBrush"); }
        }

        private Brush _cotractFranchiseBrush;

        public Brush ContractFranchiseBrush
        {
            get { return _cotractFranchiseBrush; }
            set { _cotractFranchiseBrush = value; OnPropertyChanged("ContractFranchiseBrush"); }
        }

        private Brush _contractStickerBrush;

        public Brush ContractStickerBrush
        {
            get { return _contractStickerBrush; }
            set { _contractStickerBrush = value; OnPropertyChanged("ContractStickerBrush"); }
        }


        #endregion

        #region Client Info
        private string _clientName;

        public string ClientName
        {
            get { return _clientName; }
            set { _clientName = value; OnPropertyChanged("ClientName"); }
        }

        private string _clientLastName;

        public string ClientLastName
        {
            get { return _clientLastName; }
            set { _clientLastName = value; OnPropertyChanged("ClientLastName"); }
        }

        private string _clientPatronymic;

        public string ClientPatronymic
        {
            get { return _clientPatronymic; }
            set { _clientPatronymic = value; OnPropertyChanged("ClientPatronymic"); }
        }

        private string _clientCodeID;

        public string ClientCodeID
        {
            get { return _clientCodeID; }
            set { _clientCodeID = value; OnPropertyChanged("ClientCodeID"); }
        }

        private bool _clientFlag;

        public bool ClientFlag
        {
            get { return _clientFlag; }
            set { _clientFlag = value; OnPropertyChanged("ClientFlag"); }
        }

        private string _clientPassportNumber;

        public string ClientPassportNumber
        {
            get { return _clientPassportNumber; }
            set { _clientPassportNumber = value; OnPropertyChanged("ClientPassportNumber"); }
        }

        private string _clientAddressCountry;

        public string ClientAddressCountry
        {
            get { return _clientAddressCountry; }
            set { _clientAddressCountry = value; OnPropertyChanged("ClientAddressCountry"); }
        }

        private string _clientAddressCity;

        public string ClientAddressCity
        {
            get { return _clientAddressCity; }
            set { _clientAddressCity = value; OnPropertyChanged("ClientAddressCity"); }
        }

        private string _clientAddressStreet;

        public string ClientAddressStreet
        {
            get { return _clientAddressStreet; }
            set { _clientAddressStreet = value; OnPropertyChanged("ClientAddressStreet"); }
        }

        private string _clientAddressBuilding;

        public string ClientAddressBuilding
        {
            get { return _clientAddressBuilding; }
            set { _clientAddressBuilding = value; OnPropertyChanged("ClientAddressBuilding"); }
        }

        private string _clientAddressFlat;

        public string ClientAddressFlat
        {
            get { return _clientAddressFlat; }
            set { _clientAddressFlat = value; OnPropertyChanged("ClientAddressFlat"); }
        }

        private string _clientAddressPostCode;

        public string ClientAddressPostCode
        {
            get { return _clientAddressPostCode; }
            set { _clientAddressPostCode = value; OnPropertyChanged("ClientAddressPostCode"); }
        }

        private string _clientPhone;

        public string ClientPhone
        {
            get { return _clientPhone; }
            set { _clientPhone = value; OnPropertyChanged("ClientPhone"); }
        }

        private string _clientDriversLicense;

        public string ClientDriversLicense
        {
            get { return _clientDriversLicense; }
            set { _clientDriversLicense = value; OnPropertyChanged("ClientDriversLicense"); }
        }

        private string _clientDriversExpirience;

        public string ClientDriversExpirience
        {
            get { return _clientDriversExpirience; }
            set { _clientDriversExpirience = value; OnPropertyChanged("ClientDriversExpirience"); }
        }
        #endregion

        #region Vehicle Info
        private string _vehicleBrand;

        public string VehicleBrand
        {
            get { return _vehicleBrand; }
            set { _vehicleBrand = value; OnPropertyChanged("VehicleBrand"); }
        }

        private string _vehicleNumber;

        public string VehicleNumber
        {
            get { return _vehicleNumber; }
            set { _vehicleNumber = value; OnPropertyChanged("VehicleNumber"); }
        }

        private string _vehicleYear;

        public string VehicleYear
        {
            get { return _vehicleYear; }
            set { _vehicleYear = value; OnPropertyChanged("VehicleYear"); }
        }

        private string _vehicleLicensePlate;

        public string VehicleLicense
        {
            get { return _vehicleLicensePlate; }
            set { _vehicleLicensePlate = value; OnPropertyChanged("VehicleLicense"); }
        }

        private string _vehicleValue;

        public string VehicleValue
        {
            get { return _vehicleValue; }
            set { _vehicleValue = value; OnPropertyChanged("VehicleValue"); }
        }

        private string _vehicleColor;

        public string VehicleColor
        {
            get { return _vehicleColor; }
            set { _vehicleColor = value; OnPropertyChanged("VehicleColor"); }
        }

        private string _vehicleVolume;

        public string VehicleVolume
        {
            get { return _vehicleVolume; }
            set { _vehicleVolume = value; OnPropertyChanged("VehicleVolume"); }
        }

        private string _vehicleAmountOfSeats;

        public string VehicleAmountOfSeats
        {
            get { return _vehicleAmountOfSeats; }
            set { _vehicleAmountOfSeats = value; OnPropertyChanged("VehicleAmountOfSeats"); }
        }

        private string _vehicleCarrying;

        public string VehicleCarrying
        {
            get { return _vehicleCarrying; }
            set { _vehicleCarrying = value; OnPropertyChanged("VehicleCarrying"); }
        }

        private string _vehicleCityOfRegistration;

        public string VehicleCityOfRegistration
        {
            get { return _vehicleCityOfRegistration; }
            set { _vehicleCityOfRegistration = value; OnPropertyChanged("VehicleCityOfRegistration"); }
        }

        private string _vehicleType;

        public string VehicleType
        {
            get { return _vehicleType; }
            set { _vehicleType = value; OnPropertyChanged("VehicleType"); }
        }

        #endregion

        #region Contract Info
        private string _contractNumber;

        public string ContractNumber
        {
            get { return _contractNumber; }
            set { _contractNumber = value; OnPropertyChanged("ContractNumber"); }
        }

        private string _contractCodeID;

        public string ContractCodeID
        {
            get { return _contractCodeID; }
            set { _contractCodeID = value; OnPropertyChanged("ContractCodeID"); }
        }

        private bool _contractFlag;

        public bool ContractFlag
        {
            get { return _contractFlag; }
            set { _contractFlag = value; OnPropertyChanged("ContractFlag"); }
        }

        private string _contractBrand;

        public string ContractBrand
        {
            get { return _contractBrand; }
            set { _contractBrand = value; OnPropertyChanged("ContractBrand"); }
        }

        private string _contractValue;

        public string ContractValue
        {
            get { return _contractValue; }
            set { _contractValue = value; OnPropertyChanged("ContractValue"); }
        }

        private string _contractOwnerName;

        public string ContractOwnerName
        {
            get { return _contractOwnerName; }
            set { _contractOwnerName = value; OnPropertyChanged("ContractOwnerName"); }
        }

        private string _contractFranchise;

        public string ContractFranchise
        {
            get { return _contractFranchise; }
            set { _contractFranchise = value; OnPropertyChanged("ContractFranchise"); }
        }

        private DateTime _contractBeginningDate;

        public DateTime ContractBeginningDate
        {
            get { return _contractBeginningDate; }
            set { _contractBeginningDate = value; OnPropertyChanged("ContractBeginningDate"); }
        }

        private DateTime _contractFinishingDate;

        public DateTime ContractFinishingDate
        {
            get { return _contractFinishingDate; }
            set { _contractFinishingDate = value; OnPropertyChanged("ContractFinishingDate"); }
        }

        private DateTime _contractRegistrationDate;

        public DateTime ContractRegistrationDate
        {
            get { return _contractRegistrationDate; }
            set { _contractRegistrationDate = value; OnPropertyChanged("ContractRegistrationDate"); }
        }

        private string _contractSticker;

        public string ContractSticker
        {
            get { return _contractSticker; }
            set { _contractSticker = value; OnPropertyChanged("ContractSticker"); }
        }

        #endregion

        private string _category;

        public string Category
        {
            get { return _category; }
            set { _category = value; OnPropertyChanged("Category"); }
        }

        private INode _dataForChanging;

        public INode DataForChanging
        {
            get { return _dataForChanging; }
            set { _dataForChanging = value; this.Extract(); }
        }

        private INodes _nodes;

        public INodes Nodes
        {
            get { return _nodes; }
            set { _nodes = value; }
        }
        #endregion

        #region Properties
        public ICommand OKCommand { get; set; }
        public INode NewData { get; private set; }

        private bool _isDataGenerated;

        public bool IsDataGenerated
        {
            get { return _isDataGenerated; }
            private set { _isDataGenerated = value; OnPropertyChanged("IsDataGenerated"); }
        }
        #endregion

        #region Methods
        bool CheckValues()
        {
            bool isOk = true;
            //проверка ClientCodeID int
            try
            {
                Convert.ToInt32(this.ClientCodeID);
                ClientCodeIDBrush = _gray;

            }
            catch
            {
                ClientCodeIDBrush = Brushes.Red;
                isOk = false;
            }

            //проверка ClientDriversExpirience int
            try
            {
                Convert.ToInt32(this.ClientDriversExpirience);
                СlientDriverExpirienceBrush = _gray;
            }
            catch
            {
                СlientDriverExpirienceBrush = Brushes.Red;
                isOk = false;
            }

            //проверка VehicleNumber int
            try
            {
                Convert.ToInt32(this.VehicleNumber);
                VehicleNumberBrush = _gray;
            }
            catch
            {
                VehicleNumberBrush = Brushes.Red;
                isOk = false;
            }

            //VehicleYear int
            try
            {
                Convert.ToInt32(this.VehicleYear);
                VehicleYearBrush = _gray;
            }
            catch
            {
                VehicleYearBrush = Brushes.Red;
                isOk = false;
            }

            //VehicleValue double
            try
            {
                Convert.ToDouble(this.VehicleValue);
                VehicleValueBrush = _gray;
            }
            catch
            {
                VehicleValueBrush = Brushes.Red;
                isOk = false;
            }

            //VehicleVolume double
            try
            {
                Convert.ToDouble(this.VehicleVolume);
                VehicleVolumeBrush = _gray;
            }
            catch
            {
                VehicleVolumeBrush = Brushes.Red;
                isOk = false;
            }

            //VehicleAmountOfSeats int
            try
            {
                Convert.ToInt32(this.VehicleAmountOfSeats);
                VehicleAmountOfSeatsBrush = _gray;
            }
            catch
            {
                VehicleAmountOfSeatsBrush = Brushes.Red;
                isOk = false;
            }

            //VehicleCarrying double
            try
            {
                Convert.ToDouble(this.VehicleCarrying);
                VehicleCarryingBrush = _gray;
            }
            catch
            {
                VehicleCarryingBrush = Brushes.Red;
                isOk = false;
            }

            //ContractNumber double
            try
            {
                Convert.ToDouble(this.ContractNumber);
                ContractNumberBrush = _gray;
            }
            catch
            {
                ContractNumberBrush = Brushes.Red;
                isOk = false;
            }

            //ContractCodeID int
            try
            {
                Convert.ToInt32(this.ContractCodeID);
                ContractCodeIDBrush = _gray;
            }
            catch
            {
                ContractCodeIDBrush = Brushes.Red;
                isOk = false;
            }

            //ContractValue double
            try
            {
                Convert.ToDouble(this.ContractValue);
                ContractValueBrush = _gray;
            }
            catch
            {
                ContractValueBrush = Brushes.Red;
                isOk = false;
            }

            //ContractFranchise double
            try
            {
                Convert.ToDouble(this.ContractFranchise);
                ContractFranchiseBrush = _gray;
            }
            catch
            {
                ContractFranchiseBrush = Brushes.Red;
                isOk = false;
            }

            //ContractSticker int
            try
            {
                Convert.ToInt32(this.ContractSticker);
                ContractStickerBrush = _gray;
            }
            catch
            {
                ContractStickerBrush = Brushes.Red;
                isOk = false;
            }

            if (string.IsNullOrWhiteSpace(ClientLastName) ||
                string.IsNullOrWhiteSpace(ClientName) ||
                string.IsNullOrWhiteSpace(ClientPatronymic) ||
                string.IsNullOrWhiteSpace(ClientCodeID) ||
                string.IsNullOrWhiteSpace(ClientPassportNumber) ||
                string.IsNullOrWhiteSpace(ClientPhone) ||
                string.IsNullOrWhiteSpace(ClientDriversLicense) ||
                string.IsNullOrWhiteSpace(ClientDriversExpirience) ||
                string.IsNullOrWhiteSpace(ClientAddressCountry) ||
                string.IsNullOrWhiteSpace(ClientAddressCity) ||
                string.IsNullOrWhiteSpace(ClientAddressStreet) ||
                string.IsNullOrWhiteSpace(VehicleBrand) ||
                string.IsNullOrWhiteSpace(VehicleNumber) ||
                string.IsNullOrWhiteSpace(VehicleYear) ||
                string.IsNullOrWhiteSpace(VehicleLicense) ||
                string.IsNullOrWhiteSpace(VehicleValue) ||
                string.IsNullOrWhiteSpace(VehicleColor) ||
                string.IsNullOrWhiteSpace(VehicleVolume) ||
                string.IsNullOrWhiteSpace(VehicleAmountOfSeats) ||
                string.IsNullOrWhiteSpace(VehicleCarrying) ||
                string.IsNullOrWhiteSpace(VehicleCityOfRegistration) ||
                string.IsNullOrWhiteSpace(VehicleType) ||
                string.IsNullOrWhiteSpace(ContractNumber) ||
                string.IsNullOrWhiteSpace(ContractCodeID) ||
                string.IsNullOrWhiteSpace(ContractBrand) ||
                string.IsNullOrWhiteSpace(ContractValue) ||
                string.IsNullOrWhiteSpace(ContractOwnerName) ||
                string.IsNullOrWhiteSpace(ContractFranchise) ||
                string.IsNullOrWhiteSpace(ContractSticker))
            {
                isOk = false;
                MessageBox.Show("Строки не должны быть пустыми или состоять только из пробелов!", "Упс!");
            }

            return isOk;
        }

        void GenerateData()
        {
            if (this.CheckValues())
            {
                //var newClient = new Client(ClientName, ClientLastName, ClientPatronymic, Convert.ToInt32(ClientCodeID), ClientFlag, ClientPassportNumber, ClientAddress, ClientPhone, ClientDriversLicense, Convert.ToInt32(ClientDriversExpirience));
                //var newVehicle = new Vehicle(VehicleBrand, Convert.ToInt32(VehicleNumber), Convert.ToInt32(VehicleYear), VehicleLicense, Convert.ToDouble(VehicleValue), VehicleColor, Convert.ToDouble(VehicleVolume), Convert.ToInt32(VehicleAmountOfSeats), Convert.ToDouble(VehicleCarrying), VehicleCityOfRegistration, VehicleType);
                //var newContract = new Contract(Convert.ToDouble(ContractNumber), Convert.ToInt32(ContractCodeID), ContractFlag, ContractBrand, Convert.ToDouble(ContractValue), ContractOwnerName, Convert.ToDouble(ContractFranchise), ContractBeginningDate, ContractFinishingDate, ContractRegistrationDate, Convert.ToInt32(ContractSticker));
                //var newAddress = new Address
                //{
                    
                //};

                //var newClient = new Client
                //{

                //};

                //var newVehicle = new Vehicle
                //{

                //};

                //var newContract = new Contract
                //{

                //};

                //this.NewData = new Node
                //{

                //};

                this.IsDataGenerated = true;
            }
            else
            {
                this.IsDataGenerated = false;
            }
        }

        void Extract()
        {
            this.ClientAddressBuilding = DataForChanging.Client.Address.Building.ToString();
            this.ClientAddressCity = DataForChanging.Client.Address.City.ToString();
            this.ClientAddressCountry = DataForChanging.Client.Address.Country.ToString();
            this.ClientAddressFlat = DataForChanging.Client.Address.FlatNumber.ToString();
            this.ClientAddressPostCode = DataForChanging.Client.Address.PostCode.ToString();
            this.ClientAddressStreet = DataForChanging.Client.Address.Street.ToString();
            this.ClientCodeID = DataForChanging.Client.TaxNumber.ToString();
            this.ClientDriversExpirience = DataForChanging.Client.DriversExpirience.ToString();
            this.ClientDriversLicense = DataForChanging.Client.DriversLicenseNumber;
            this.ClientFlag = DataForChanging.Client.IsPhysicalPerson;
            this.ClientLastName = DataForChanging.Client.LastName;
            this.ClientName = DataForChanging.Client.FirstName;
            this.ClientPassportNumber = DataForChanging.Client.Passport;
            this.ClientPatronymic = DataForChanging.Client.MiddleName;
            this.ClientPhone = DataForChanging.Client.Phone;
            this.ContractBeginningDate = DataForChanging.Contract.BeginningDate;
            this.ContractBrand = DataForChanging.Contract.Brand;
            this.ContractCodeID = DataForChanging.Contract.TaxNumber.ToString();
            this.ContractFinishingDate = DataForChanging.Contract.FinishingDate;
            this.ContractFlag = DataForChanging.Contract.IsPhysicalPerson;
            this.ContractFranchise = DataForChanging.Contract.Franchise.ToString();
            this.ContractNumber = DataForChanging.Contract.ContractNumber.ToString();
            this.ContractOwnerName = DataForChanging.Contract.VehicleOwnerName;
            this.ContractRegistrationDate = DataForChanging.Contract.RegistrationDate;
            this.ContractSticker = DataForChanging.Contract.Sticker.ToString();
            this.ContractValue = DataForChanging.Contract.Value.ToString();
            this.VehicleAmountOfSeats = DataForChanging.Vehicle.AmountOfSeats.ToString();
            this.VehicleBrand = DataForChanging.Vehicle.Brand;
            this.VehicleCarrying = DataForChanging.Vehicle.Carrying.ToString();
            this.VehicleCityOfRegistration = DataForChanging.Vehicle.CityOfRegistration;
            this.VehicleColor = DataForChanging.Vehicle.Color;
            this.VehicleLicense = DataForChanging.Vehicle.LicensePlate;
            this.VehicleNumber = DataForChanging.Vehicle.Number.ToString();
            this.VehicleType = DataForChanging.Vehicle.Type;
            this.VehicleValue = DataForChanging.Vehicle.Value.ToString();
            this.VehicleVolume = DataForChanging.Vehicle.Volume.ToString();
            this.VehicleYear = DataForChanging.Vehicle.Year.ToString();
            this.Category = DataForChanging.Category.Name;
        }
        #endregion
    }
}
