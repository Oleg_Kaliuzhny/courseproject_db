﻿using BLL.Contracts;
using BLL.Contracts.DTO;
using kursGUI.View;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace kursGUI.ViewModel
{
    public class VehiclesPageViewModel : ViewModelBase
    {
        #region Constructor
        public VehiclesPageViewModel(INodes nodes)
        {
            _nodes = nodes;
            Collection = new ObservableCollection<IVehicle>();
            this.FormVehiclesCollection(nodes);

            this.BeginSearchCommand = new Command(arg => Search());
            this.UpdateVehiclesCommand = new Command(arg => FormVehiclesCollection(nodes));
            this.AddVehicleCommand = new Command(arg => Add());
            this.EditVehicleCommand = new Command(arg =>
            {
                if (SelectedItem != null)
                    Add(SelectedItem);
            });
            Collection.CollectionChanged += CollectionCollectionChanged;
        }
        #endregion

        #region Fields
        INodes _nodes;
        #endregion

        #region Properties
        #region Search
        private string _searchQuery;

        public string SearchQuery
        {
            get { return _searchQuery; }
            set { _searchQuery = value; this.Search(); }
        }

        private string _searchValueFrom;

        public string SearchValueFrom
        {
            get { return _searchValueFrom; }
            set { _searchValueFrom = value; /* this.Search(); */}
        }

        private string _searchValueTo;

        public string SearchValueTo
        {
            get { return _searchValueTo; }
            set { _searchValueTo = value; /* this.Search(); */}
        }

        private bool _searchValueLessThen300;

        public bool SearchValueLessThen300
        {
            get { return _searchValueLessThen300; }
            set { _searchValueLessThen300 = value; /* this.Search(); */}
        }

        #endregion

        public IVehicle SelectedItem { get; set; }

        private ObservableCollection<IVehicle> _collection;

        public ObservableCollection<IVehicle> Collection
        {
            get { return _collection; }
            set { _collection = value; OnPropertyChanged("Collection"); }
        }

        public ICommand BeginSearchCommand { get; set; }
        public ICommand UpdateVehiclesCommand { get; set; }
        public ICommand AddVehicleCommand { get; set; }
        public ICommand EditVehicleCommand { get; set; }

        #endregion

        #region Methods
        public void FormVehiclesCollection(INodes nodes)
        {
            Collection.Clear();
            var items = nodes.GetVehicles(0, int.MaxValue);
            foreach (var item in items)
            {
                Collection.Add(item);
            }
        }

        async void Search()
        {
            Collection.Clear();

            var to = double.TryParse(SearchValueTo, out double result)
                ? result : SearchValueLessThen300 ? 300 : double.MaxValue;

            var items = await Task.Run(() => _nodes.SearchVehicles(SearchQuery ?? "", to, 0, int.MaxValue));

            foreach (var item in items)
            {
                Collection.Add(item);
            }
        }

        private void CollectionCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove)
            {
                foreach (var item in e.OldItems)
                {
                    _nodes.DeleteVehicle((item as IVehicle).Id);
                }
            }
        }

        private void Add(IVehicle vehicle = null)
        {
            var addViewModel = new AddEditViewModels.AddEditVehicleViewModel();

            if (vehicle != null)
            {
                addViewModel.Id = vehicle.Id;
                addViewModel.VehicleAmountOfSeats = vehicle.AmountOfSeats.ToString();
                addViewModel.VehicleBrand = vehicle.Brand;
                addViewModel.VehicleCarrying = vehicle.Carrying.ToString();
                addViewModel.VehicleCityOfRegistration = vehicle.CityOfRegistration;
                addViewModel.VehicleColor = vehicle.Color;
                addViewModel.VehicleLicense = vehicle.LicensePlate;
                addViewModel.VehicleNumber = vehicle.Number.ToString();
                addViewModel.VehicleType = vehicle.Type;
                addViewModel.VehicleValue = vehicle.Value.ToString();
                addViewModel.VehicleVolume = vehicle.Volume.ToString();
                addViewModel.VehicleYear = vehicle.Year.ToString();
            }

            var addView = new AddVehicle();
            addView.DataContext = addViewModel;
            addView.ShowDialog();

            if (addViewModel.IsGenerated)
            {
                _nodes.SaveVehicle(addViewModel.GeneratedInstance);
                FormVehiclesCollection(_nodes);
            }
        }
        #endregion
    }
}
