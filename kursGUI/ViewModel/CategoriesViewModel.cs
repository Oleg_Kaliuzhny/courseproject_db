﻿using BLL.Contracts;
using BLL.Contracts.DTO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace kursGUI.ViewModel
{
    public class CategoriesViewModel : ViewModelBase
    {
        #region Constructor
        public CategoriesViewModel(INodes nodes)
        {
            this._nodes = nodes;
            this.CategoriesCollection = new ObservableCollection<ICategory>();
            this.FormCategoriesCollection(nodes);

            this.BeginSearchCommand = new Command(arg => Search());
            this.UpdateCategoriesCommand = new Command(arg => FormCategoriesCollection(nodes));
            this.AddCategoryCommand = new Command(arg => Add());
            this.EditCategoryCommand = new Command(arg => 
            {
                if (SelectedItem != null)
                    Add(SelectedItem);
            });
            CategoriesCollection.CollectionChanged += CategoriesCollectionCollectionChanged;
        }
        #endregion

        #region Fields
        INodes _nodes;
        #endregion

        #region Properties
        public ICategory SelectedItem { get; set; }

        private string _searchQuery;

        public string SearchQuery
        {
            get { return _searchQuery; }
            set { _searchQuery = value; OnPropertyChanged("SearchQuery"); this.Search(); }
        }

        private ObservableCollection<ICategory> _categoriesCollection;

        public ObservableCollection<ICategory> CategoriesCollection
        {
            get { return _categoriesCollection; }
            set { _categoriesCollection = value; OnPropertyChanged("CategoriesCollection"); }
        }

        public ICommand BeginSearchCommand { get; set; }
        public ICommand UpdateCategoriesCommand { get; set; }
        public ICommand AddCategoryCommand { get; set; }
        public ICommand EditCategoryCommand { get; set; }
        #endregion

        #region Methods
        public async void FormCategoriesCollection(INodes nodes)
        {
            CategoriesCollection.Clear();
            IEnumerable<ICategory> items = await Task.Run(() => nodes.GetCategories(0, int.MaxValue));

            foreach (var item in items)
            {
                CategoriesCollection.Add(item);
            }
        }

        async void Search()
        {
            CategoriesCollection.Clear();
            IEnumerable<ICategory> items = await Task.Run(() => _nodes.SearchCategories(SearchQuery ?? "", 0, int.MaxValue));

            foreach (var item in items)
            {
                CategoriesCollection.Add(item);
            }
        }

        private void CategoriesCollectionCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove)
            {
                foreach (var item in e.OldItems)
                {
                    _nodes.DeleteCategory((item as ICategory).Id);
                }
            }
        }

        private void Add(ICategory category = null)
        {
            var addViewModel = new AddEditViewModels.AddEditCategoryViewModel();

            if (category != null)
            {
                addViewModel.Title = category.Name;
                addViewModel.Description = category.Description;
                addViewModel.Id = category.Id;
            }

            var addView = new View.AddCategory();
            addView.DataContext = addViewModel;
            addView.ShowDialog();

            if (addViewModel.IsGenerated)
            {
                _nodes.SaveCategory(addViewModel.GeneratedInstance);
                FormCategoriesCollection(_nodes);
            }
        }
        #endregion
    }
}
