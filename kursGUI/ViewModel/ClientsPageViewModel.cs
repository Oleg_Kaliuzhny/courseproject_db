﻿using BLL.Contracts;
using BLL.Contracts.DTO;
using BLL.Domains.DTO;
using kursGUI.View;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace kursGUI.ViewModel
{
    public class ClientsPageViewModel : ViewModelBase
    {
        #region Constructor
        public ClientsPageViewModel(INodes nodes)
        {
            _nodes = nodes;
            Collection = new ObservableCollection<IClient>();
            this.FormContractsCollection(nodes);

            this.BeginSearchCommand = new Command(arg => Search());
            this.UpdateCommand = new Command(arg => FormContractsCollection(nodes));
            this.AddCommand = new Command(arg => Add());
            this.EditCommand = new Command(arg =>
            {
                if (SelectedItem != null)
                    Add(SelectedItem);
            });
            Collection.CollectionChanged += CollectionCollectionChanged;
        }
        #endregion

        #region Fields
        DataWindowViewModel _dataWindowViewModel;
        DataWindow _dataWindow;
        INodes _nodes;
        #endregion

        #region Properties
        #region Search
        private string _searchQuery;

        public string SearchQuery
        {
            get { return _searchQuery; }
            set { _searchQuery = value; this.Search(); }
        }

        #endregion

        public IClient SelectedItem { get; set; }

        private ObservableCollection<IClient> _collection;

        public ObservableCollection<IClient> Collection
        {
            get { return _collection; }
            set { _collection = value; OnPropertyChanged("Collection"); }
        }

        public ICommand BeginSearchCommand { get; set; }
        public ICommand UpdateCommand { get; set; }
        public ICommand AddCommand { get; set; }
        public ICommand EditCommand { get; set; }

        #endregion

        #region Methods
        public  void FormContractsCollection(INodes nodes)
        {
            Collection.Clear();
            var items = nodes.GetClients(0, int.MaxValue);
            foreach (var item in items)
            {
                Collection.Add(item);
            }
        }

        async void Search()
        {
            Collection.Clear();
            var items = await Task.Run(() => _nodes.SearchClients(SearchQuery ?? "", 0, int.MaxValue));

            foreach (var item in items)
            {
                Collection.Add(item);
            }
        }

        private void CollectionCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove)
            {
                foreach (var item in e.OldItems)
                {
                    _nodes.DeleteContract((item as IContract).Id);
                }
            }
        }

        private void Add(IClient client = null)
        {
            var addViewModel = new AddEditViewModels.AddEditClientViewModel();

            if (client != null)
            {
                addViewModel.Id = client.Id;
                addViewModel.ClientAddressBuilding = client.Address.Building.ToString();
                addViewModel.ClientAddressCity = client.Address.City.ToString();
                addViewModel.ClientAddressCountry = client.Address.Country;
                addViewModel.ClientAddressFlat = client.Address.FlatNumber.ToString();
                addViewModel.ClientAddressPostCode = client.Address.PostCode.ToString();
                addViewModel.ClientAddressStreet = client.Address.Street;
                addViewModel.ClientCodeID = client.TaxNumber.ToString();
                addViewModel.ClientDriversExpirience = client.DriversExpirience.ToString();
                addViewModel.ClientDriversLicense = client.DriversLicenseNumber.ToString();
                addViewModel.ClientFlag = client.IsPhysicalPerson;
                addViewModel.ClientLastName = client.LastName.ToString();
                addViewModel.ClientName = client.FirstName.ToString();
                addViewModel.ClientPassportNumber = client.Passport.ToString();
                addViewModel.ClientPatronymic = client.MiddleName.ToString();
                addViewModel.ClientPhone = client.Phone.ToString();
            }

            var addView = new AddClient();
            addView.DataContext = addViewModel;
            addView.ShowDialog();

            if (addViewModel.IsGenerated)
            {
                var addressId = _nodes.SaveAddress(addViewModel.GeneratedInstance.Address);
                ((Client)addViewModel.GeneratedInstance).AddressId = addressId;
                _nodes.SaveClient(addViewModel.GeneratedInstance);
                FormContractsCollection(_nodes);
            }
        }
        #endregion
    }
}
