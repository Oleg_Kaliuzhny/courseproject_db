﻿using System;
using System.Windows.Input;

namespace kursGUI.ViewModel
{
    public class MenuPageViewModel : ViewModelBase
    {
        #region Constructor
        public MenuPageViewModel()
        {
            ToDataPageCommand = new Command(arg => DataPage());
            ToClientsPageCommand = new Command(arg => ClientsPage());
            ToVehiclesPageCommand = new Command(arg => VehiclesPage());
            ToContractsPageCommand = new Command(arg => ContractsPage());
            ToCategoriesPageCommand = new Command(arg => CategoriesPage());
            ToGraphPageCommand = new Command(arg => GraphPage());
        }
        #endregion

        #region Properties
        public ICommand ToDataPageCommand { get; set; }
        public ICommand ToClientsPageCommand { get; set; }
        public ICommand ToVehiclesPageCommand { get; set; }
        public ICommand ToContractsPageCommand { get; set; }
        public ICommand ToCategoriesPageCommand { get; set; }
        public ICommand ToGraphPageCommand { get; set; }
        #endregion

        #region Methods
        void DataPage()
        {
            PageSelected?.Invoke("DataPage");
        }

        void ClientsPage()
        {
            PageSelected?.Invoke("ClientsPage");
        }

        void VehiclesPage()
        {
            PageSelected?.Invoke("VehiclesPage");
        }

        void ContractsPage()
        {
            PageSelected?.Invoke("ContractsPage");
        }

        void CategoriesPage()
        {
            PageSelected?.Invoke("CategoriesPage");
        }

        void GraphPage()
        {
            PageSelected?.Invoke("GraphPage");
        }
        #endregion

        #region Events
        public event Action<string> PageSelected;
        #endregion
    }
}
