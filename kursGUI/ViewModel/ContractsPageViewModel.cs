﻿using BLL.Contracts;
using BLL.Contracts.DTO;
using kursGUI.View;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;

namespace kursGUI.ViewModel
{
    public class ContractsPageViewModel : ViewModelBase
    {
        #region Constructor
        public ContractsPageViewModel(INodes nodes)
        {
            _nodes = nodes;
            Collection = new ObservableCollection<IContract>();
            this.FormContractsCollection(nodes);

            this.BeginSearchCommand = new Command(arg => Search());
            this.UpdateContractsCommand = new Command(arg => FormContractsCollection(nodes));
            this.AddContractCommand = new Command(arg => Add());
            this.EditContractCommand = new Command(arg =>
            {
                if (SelectedItem != null)
                    Add(SelectedItem);
            });
            Collection.CollectionChanged += CollectionCollectionChanged;
        }
        #endregion

        #region Fields
        INodes _nodes;
        #endregion

        #region Properties
        #region Search
        private string _searchQuery;

        public string SearchQuery
        {
            get { return _searchQuery; }
            set { _searchQuery = value; this.Search(); }
        }

        private string _searchValueFrom;

        public string SearchValueFrom
        {
            get { return _searchValueFrom; }
            set { _searchValueFrom = value; /* this.Search(); */}
        }

        private string _searchValueTo;

        public string SearchValueTo
        {
            get { return _searchValueTo; }
            set { _searchValueTo = value; /* this.Search(); */}
        }

        private bool _searchValueLessThen300;

        public bool SearchValueLessThen300
        {
            get { return _searchValueLessThen300; }
            set { _searchValueLessThen300 = value; /* this.Search(); */}
        }

        #endregion

        public IContract SelectedItem { get; set; }

        private ObservableCollection<IContract> _collection;

        public ObservableCollection<IContract> Collection
        {
            get { return _collection; }
            set { _collection = value; OnPropertyChanged("Collection"); }
        }

        public ICommand BeginSearchCommand { get; set; }
        public ICommand UpdateContractsCommand { get; set; }
        public ICommand AddContractCommand { get; set; }
        public ICommand EditContractCommand { get; set; }

        #endregion

        #region Methods
        public void FormContractsCollection(INodes nodes)
        {
            Collection.Clear();
            var items = nodes.GetContracts(0, int.MaxValue);
            foreach (var item in items)
            {
                Collection.Add(item);
            }
        }

        async void Search()
        {
            Collection.Clear();

            var from = double.TryParse(SearchValueFrom, out double result) ? result : 0.0d;
            var to = double.TryParse(SearchValueTo, out result) 
                ? result : SearchValueLessThen300 ? 300 : double.MaxValue;

            var items = await Task.Run(() => _nodes.SearchContracts(SearchQuery ?? "", from, to, 0, int.MaxValue));

            foreach (var item in items)
            {
                Collection.Add(item);
            }
        }

        private void CollectionCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove)
            {
                foreach (var item in e.OldItems)
                {
                    _nodes.DeleteContract((item as IContract).Id);
                }
            }
        }

        private void Add(IContract contract = null)
        {
            var addViewModel = new AddEditViewModels.AddEditContractViewModel();

            if (contract != null)
            {
                addViewModel.Id = contract.Id;
                addViewModel.ContractBeginningDate = contract.BeginningDate;
                addViewModel.ContractBrand = contract.Brand;
                addViewModel.ContractFinishingDate = contract.FinishingDate;
                addViewModel.ContractFranchise = contract.Franchise.ToString();
                addViewModel.ContractFlag = contract.IsPhysicalPerson;
                addViewModel.ContractRegistrationDate = contract.RegistrationDate;
                addViewModel.ContractSticker = contract.Sticker.ToString();
                addViewModel.ContractCodeID = contract.TaxNumber.ToString();
                addViewModel.ContractValue = contract.Value.ToString();
                addViewModel.ContractOwnerName = contract.VehicleOwnerName;
                addViewModel.ContractNumber = contract.ContractNumber.ToString();
            }

            var addView = new AddContract();
            addView.DataContext = addViewModel;
            addView.ShowDialog();

            if (addViewModel.IsGenerated)
            {
                _nodes.SaveContract(addViewModel.GeneratedInstance);
                FormContractsCollection(_nodes);
            }
        }
        #endregion
    }
}
