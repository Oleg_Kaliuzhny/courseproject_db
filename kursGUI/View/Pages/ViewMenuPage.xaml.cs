﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace kursGUI.View.Pages
{
    /// <summary>
    /// Логика взаимодействия для ViewMenuPage.xaml
    /// </summary>
    public partial class ViewMenuPage : Page
    {
        public ViewMenuPage()
        {
            InitializeComponent();
        }

        internal event RoutedEventHandler Click;

        private void btnClick(object sender, RoutedEventArgs e)
        {
            this.Click?.Invoke(sender, e);
        }
    }
}
