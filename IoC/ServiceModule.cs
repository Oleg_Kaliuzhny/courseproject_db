﻿using Ninject.Modules;
using DAL.Contracts.Repositories;
using DAL.Domains.Repositories;
using BLL.Contracts;
using BLL.Domains;

namespace IoC
{
    public class ServiceModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IAddressRepository>().To<AddressRepository>();
            Bind<ICategoryRepository>().To<CategoryRepository>();
            Bind<IClientRepository>().To<ClientRepository>();
            Bind<IContractRepository>().To<ContractRepository>();
            Bind<INodeRepository>().To<NodeRepository>();
            Bind<IVehicleRepository>().To<VehicleRepository>();

            Bind<INodes>().To<Nodes>()
                .WithConstructorArgument("nodeRepository", new NodeRepository())
                .WithConstructorArgument("clientRepository", new ClientRepository())
                .WithConstructorArgument("vehicleRepository", new VehicleRepository())
                .WithConstructorArgument("contractRepository", new ContractRepository())
                .WithConstructorArgument("addressRepository", new AddressRepository())
                .WithConstructorArgument("categoryRepository", new CategoryRepository());
        }
    }
}
