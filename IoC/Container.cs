﻿using BLL.Contracts;
using Ninject;

namespace IoC
{
    public class Container
    {
        public Container()
        {
            _kernel = new StandardKernel(new ServiceModule());
        }

        private readonly IKernel _kernel;

        public INodes GetNodesManager()
        {
            return _kernel.Get<INodes>();
        }
    }
}
