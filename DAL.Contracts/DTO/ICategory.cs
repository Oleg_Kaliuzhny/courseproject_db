﻿namespace DAL.Contracts.DTO
{
    public interface ICategory : IDTOBase
    {
        string Description { get; }
        string Name { get; }
    }
}