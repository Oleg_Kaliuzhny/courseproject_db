﻿using System;

namespace DAL.Contracts.DTO
{
    public interface IContract : IDTOBase
    {
        int ContractNumber { get; set; }
        DateTime BeginningDate { get; }
        DateTime FinishingDate { get; }
        double Franchise { get; }
        bool IsPhysicalPerson { get; }
        DateTime RegistrationDate { get; }
        int Sticker { get; }
        int TaxNumber { get; }
        double Value { get; }
        string VehicleOwnerName { get; set; }
        string Brand { get; set; }
    }
}