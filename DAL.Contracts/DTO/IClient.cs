﻿namespace DAL.Contracts.DTO
{
    public interface IClient : IDTOBase
    {
        int AddressId { get; }
        int DriversExpirience { get; }
        string DriversLicenseNumber { get; }
        string FirstName { get; }
        bool IsPhysicalPerson { get; }
        string LastName { get; }
        string MiddleName { get; }
        string Passport { get; }
        string Phone { get; }
        int TaxNumber { get; }
    }
}