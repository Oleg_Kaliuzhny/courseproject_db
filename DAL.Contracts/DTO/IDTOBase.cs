﻿namespace DAL.Contracts.DTO
{
    public interface IDTOBase
    {
        int Id { get; }
    }
}