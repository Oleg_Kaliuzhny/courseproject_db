﻿namespace DAL.Contracts.DTO
{
    public interface INode : IDTOBase
    {
        int CategoryId { get; }
        int ClientId { get; }
        int ContractId { get; }
        int VehicleId { get; }
    }
}