﻿using System.Collections.Generic;
using DAL.Contracts.DTO;

namespace DAL.Contracts.Repositories
{
    public interface ICategoryRepository
    {
        int CreateCategory(ICategory model);
        void DeleteCategory(int id);
        IEnumerable<ICategory> ReadCategories(int skip, int take);
        IEnumerable<ICategory> SearchCategories(string keyword, int skip, int take);
        ICategory ReadCategoryById(int id);
        void UpdateCategory(ICategory model);
    }
}