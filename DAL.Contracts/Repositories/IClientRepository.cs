﻿using System.Collections.Generic;
using DAL.Contracts.DTO;

namespace DAL.Contracts.Repositories
{
    public interface IClientRepository
    {
        int CreateClient(IClient model);
        void DeleteClient(int id);
        IClient ReadClientById(int id);
        IEnumerable<IClient> ReadClients(int skip, int take);
        IEnumerable<IClient> SearchClients(string keyword, int skip, int take);
        void UpdateClient(IClient model);
    }
}