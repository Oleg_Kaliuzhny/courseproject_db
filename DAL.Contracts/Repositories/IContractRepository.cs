﻿using System.Collections.Generic;
using DAL.Contracts.DTO;

namespace DAL.Contracts.Repositories
{
    public interface IContractRepository
    {
        int CreateContract(IContract model);
        void DeleteContract(int id);
        IEnumerable<IContract> ReadContracts(int skip, int take);
        IEnumerable<IContract> SearchContracts(string keyword, double more, double less,
            int skip, int take);
        IContract ReadContractById(int id);
        void UpdateContract(IContract model);
    }
}