﻿using System.Collections.Generic;
using DAL.Contracts.DTO;

namespace DAL.Contracts.Repositories
{
    public interface INodeRepository
    {
        int CreateNode(INode model);
        void DeleteNode(int id);
        IEnumerable<INode> ReadNodes(int skip, int take);
        INode ReadNodeById(int id);
        void UpdateNode(INode model);
    }
}