﻿using System.Collections.Generic;
using DAL.Contracts.DTO;

namespace DAL.Contracts.Repositories
{
    public interface IAddressRepository
    {
        int CreateAddress(IAddress model);
        void DeleteAddress(int id);
        IAddress ReadAddressById(int id);
        IEnumerable<IAddress> ReadAddresses(int skip, int take);
        void UpdateAddress(IAddress model);
    }
}