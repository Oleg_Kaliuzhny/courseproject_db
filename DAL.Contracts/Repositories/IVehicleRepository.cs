﻿using System.Collections.Generic;
using DAL.Contracts.DTO;

namespace DAL.Contracts.Repositories
{
    public interface IVehicleRepository
    {
        int CreateVehicle(IVehicle model);
        void DeleteVehicle(int id);
        IEnumerable<IVehicle> ReadVehicles(int skip, int take);
        IEnumerable<IVehicle> SearchVehicles(string keyword, double less, int skip, int take);
        IVehicle ReadVehicleById(int id);
        void UpdateVehicle(IVehicle model);
    }
}