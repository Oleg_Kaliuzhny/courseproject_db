﻿namespace BLL.Contracts.DTO
{
    public interface IVehicle : IDTOBase
    {
        int AmountOfSeats { get; set; }
        string Brand { get; set; }
        double Carrying { get; set; }
        string CityOfRegistration { get; set; }
        string Color { get; set; }
        string LicensePlate { get; set; }
        int Number { get; set; }
        string Type { get; set; }
        double Value { get; set; }
        double Volume { get; set; }
        int Year { get; set; }
    }
}