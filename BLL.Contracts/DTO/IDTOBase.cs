﻿namespace BLL.Contracts.DTO
{
    public interface IDTOBase
    {
        int Id { get; }
    }
}