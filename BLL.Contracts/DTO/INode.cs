﻿namespace BLL.Contracts.DTO
{
    public interface INode : IDTOBase
    {
        int CategoryId { get; }
        int ClientId { get; }
        int ContractId { get; }
        int VehicleId { get; }
        ICategory Category { get; }
        IClient Client { get; }
        IContract Contract { get; }
        IVehicle Vehicle { get; }
        string CategoryValue { get; }
        string ClientValue { get; }
        string ContractValue { get; }
        string VehicleValue { get; }
    }
}