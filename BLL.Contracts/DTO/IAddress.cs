﻿namespace BLL.Contracts.DTO
{
    public interface IAddress : IDTOBase
    {
        int Building { get; }
        string City { get; }
        string Country { get; }
        string Street { get; set; }
        int? FlatNumber { get; }
        int PostCode { get; }
    }
}