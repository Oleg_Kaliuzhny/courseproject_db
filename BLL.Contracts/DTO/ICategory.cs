﻿namespace BLL.Contracts.DTO
{
    public interface ICategory : IDTOBase
    {
        string Description { get; set; }
        string Name { get; set; }
    }
}