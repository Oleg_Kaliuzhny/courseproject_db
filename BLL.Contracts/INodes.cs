﻿using System.Collections.Generic;
using BLL.Contracts.DTO;

namespace BLL.Contracts
{
    public interface INodes
    {
        IEnumerable<INode> GetNodes(int page, int count);
        IEnumerable<IClient> GetClients(int page, int count);
        IEnumerable<IVehicle> GetVehicles(int page, int count);
        IEnumerable<IContract> GetContracts(int page, int count);
        IEnumerable<ICategory> GetCategories(int page, int count);
        IEnumerable<INode> SearchNodes(string key, int page, int count);
        IEnumerable<IClient> SearchClients(string keyword, int page, int count);
        IEnumerable<IVehicle> SearchVehicles(string key, double less, int page, int count);
        IEnumerable<IContract> SearchContracts(string key, double more, double less, int page, int count);
        IEnumerable<ICategory> SearchCategories(string key, int page, int count);
        int SaveNode(INode node);
        int SaveClient(IClient client);
        int SaveVehicle(IVehicle vehicle);
        int SaveContract(IContract contract);
        int SaveCategory(ICategory category);
        int SaveAddress(IAddress address);
        void DeleteNode(int id);
        void DeleteClient(int id);
        void DeleteVehicle(int id);
        void DeleteContract(int id);
        void DeleteCategory(int id);
        void DeleteAddress(int id);
    }
}