﻿using System.Collections.Generic;
using System.Linq;
using DAL.Contracts.DTO;
using DAL.Contracts.Repositories;
using DAL.Domains.Entities;

namespace DAL.Domains.Repositories
{
    public class ClientRepository : BaseRepository<Entities.Client>, IClientRepository
    {
        public int CreateClient(IClient model)
        {
            return base.Create(DTOToEntity(model));
        }

        public void DeleteClient(int id)
        {
            base.Delete(id);
        }

        public IEnumerable<IClient> ReadClients(int skip, int take)
        {
            var selected = base.Read(x => true, skip, take);
            return selected.Select(x => EntityToDTO(x));
        }

        public IEnumerable<IClient> SearchClients(string keyword, int skip, int take)
        {
            var selected = base.Read(x => x.LastName.ToLower().Contains(keyword) 
            || x.FirstName.ToLower().Contains(keyword)
            || x.MiddleName.ToLower().Contains(keyword),
            skip, take);
            return selected.Select(x => EntityToDTO(x));
        }

        public IClient ReadClientById(int id)
        {
            var entity = base.Read(x => x.Id == id, 0, 1).FirstOrDefault();
            return entity == null ? null : EntityToDTO(entity);
        }

        public void UpdateClient(IClient model)
        {
            base.Update(DTOToEntity(model));
        }

        private IClient EntityToDTO(Entities.Client item)
        {
            return new DTO.Client
            {
                Id = item.Id,
                AddressId = item.AddressId,
                DriversExpirience = item.DriversExpirience,
                DriversLicenseNumber = item.DriversLicenseNumber,
                FirstName = item.FirstName,
                IsPhysicalPerson = item.IsPhysicalPerson,
                LastName = item.LastName,
                MiddleName = item.MiddleName,
                Passport = item.Passport,
                Phone = item.Phone,
                TaxNumber = item.TaxNumber,
            };
        }

        private Entities.Client DTOToEntity(IClient item)
        {
            return new Entities.Client
            {
                Id = item.Id,
                AddressId = item.AddressId,
                DriversExpirience = item.DriversExpirience,
                DriversLicenseNumber = item.DriversLicenseNumber,
                FirstName = item.FirstName,
                IsPhysicalPerson = item.IsPhysicalPerson,
                LastName = item.LastName,
                MiddleName = item.MiddleName,
                Passport = item.Passport,
                Phone = item.Phone,
                TaxNumber = item.TaxNumber,
            };
        }

        protected override void MapForUpdate(Client selected, Client entity)
        {
            if (selected == null || entity == null)
                return;
            selected.Id = entity.Id;
            selected.AddressId = entity.AddressId;
            selected.DriversExpirience = entity.DriversExpirience;
            selected.DriversLicenseNumber = entity.DriversLicenseNumber;
            selected.FirstName = entity.FirstName;
            selected.IsPhysicalPerson = entity.IsPhysicalPerson;
            selected.LastName = entity.LastName;
            selected.MiddleName = entity.MiddleName;
            selected.Passport = entity.Passport;
            selected.Phone = entity.Phone;
            selected.TaxNumber = entity.TaxNumber;
        }
    }
}
