﻿using System.Collections.Generic;
using System.Linq;
using DAL.Contracts.DTO;
using DAL.Contracts.Repositories;
using DAL.Domains.Entities;

namespace DAL.Domains.Repositories
{
    public class ContractRepository : BaseRepository<Entities.Contract>, IContractRepository
    {
        public int CreateContract(IContract model)
        {
            return base.Create(DTOToEntity(model));
        }

        public void DeleteContract(int id)
        {
            base.Delete(id);
        }

        public IEnumerable<IContract> ReadContracts(int skip, int take)
        {
            var selected = base.Read(x => true, skip, take);
            return selected.Select(x => EntityToDTO(x));
        }

        public IEnumerable<IContract> SearchContracts(string keyword, double more, double less,
            int skip, int take)
        {
            var selected = base.Read(x => x.VehicleOwnerName.ToLower().Contains(keyword)
            && less > x.Value && x.Value > more, skip, take);
            return selected.Select(x => EntityToDTO(x));
        }

        public IContract ReadContractById(int id)
        {
            var entity = base.Read(x => x.Id == id, 0, 1).FirstOrDefault();
            return entity == null ? null : EntityToDTO(entity);
        }

        public void UpdateContract(IContract model)
        {
            base.Update(DTOToEntity(model));
        }

        private IContract EntityToDTO(Entities.Contract item)
        {
            return new DTO.Contract
            {
                Id = item.Id,
                BeginningDate = item.BeginningDate,
                Brand = item.Brand,
                FinishingDate = item.FinishingDate,
                Franchise = item.Franchise,
                IsPhysicalPerson = item.IsPhysicalPerson,
                RegistrationDate = item.RegistrationDate,
                Sticker = item.Sticker,
                TaxNumber = item.TaxNumber,
                Value = item.Value,
                VehicleOwnerName = item.VehicleOwnerName,
                ContractNumber = item.ContractNumber
            };
        }

        private Entities.Contract DTOToEntity(IContract item)
        {
            return new Entities.Contract
            {
                Id = item.Id,
                BeginningDate = item.BeginningDate,
                Brand = item.Brand,
                FinishingDate = item.FinishingDate,
                Franchise = item.Franchise,
                IsPhysicalPerson = item.IsPhysicalPerson,
                RegistrationDate = item.RegistrationDate,
                Sticker = item.Sticker,
                TaxNumber = item.TaxNumber,
                Value = item.Value,
                VehicleOwnerName = item.VehicleOwnerName,
                ContractNumber = item.ContractNumber
            };
        }

        protected override void MapForUpdate(Contract selected, Contract entity)
        {
            if (selected == null || entity == null)
                return;
            selected.Id = entity.Id;
            selected.BeginningDate = entity.BeginningDate;
            selected.Brand = entity.Brand;
            selected.FinishingDate = entity.FinishingDate;
            selected.Franchise = entity.Franchise;
            selected.IsPhysicalPerson = entity.IsPhysicalPerson;
            selected.RegistrationDate = entity.RegistrationDate;
            selected.Sticker = entity.Sticker;
            selected.TaxNumber = entity.TaxNumber;
            selected.Value = entity.Value;
            selected.VehicleOwnerName = entity.VehicleOwnerName;
            selected.ContractNumber = entity.ContractNumber;
        }
    }
}
