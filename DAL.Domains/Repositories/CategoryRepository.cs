﻿using System.Collections.Generic;
using System.Linq;
using DAL.Contracts.DTO;
using DAL.Contracts.Repositories;
using DAL.Domains.Entities;

namespace DAL.Domains.Repositories
{
    public class CategoryRepository : BaseRepository<Entities.Category>, ICategoryRepository
    {
        public int CreateCategory(ICategory model)
        {
            return base.Create(DTOToEntity(model));
        }

        public void DeleteCategory(int id)
        {
            base.Delete(id);
        }

        public IEnumerable<ICategory> ReadCategories(int skip, int take)
        {
            var selected = base.Read(x => true, skip, take);
            return selected.Select(x => EntityToDTO(x));
        }

        public IEnumerable<ICategory> SearchCategories(string keyword, int skip, int take)
        {
            var selected = base.Read(x => x.Name.ToLower().Contains(keyword), skip, take);
            return selected.Select(x => EntityToDTO(x));
        }

        public ICategory ReadCategoryById(int id)
        {
            var entity = base.Read(x => x.Id == id, 0, 1).FirstOrDefault();
            return entity == null ? null : EntityToDTO(entity);
        }

        public void UpdateCategory(ICategory model)
        {
            base.Update(DTOToEntity(model));
        }

        private ICategory EntityToDTO(Entities.Category item)
        {
            return new DTO.Category
            {
                Id = item.Id,
                Description = item.Description,
                Name = item.Name,
            };
        }

        private Entities.Category DTOToEntity(ICategory item)
        {
            return new Entities.Category
            {
                Id = item.Id,
                Description = item.Description,
                Name = item.Name,
            };
        }

        protected override void MapForUpdate(Category selected, Category entity)
        {
            if (selected == null || entity == null)
                return;
            selected.Id = entity.Id;
            selected.Name = entity.Name;
            selected.Description = entity.Description;
        }
    }
}
