﻿using System.Collections.Generic;
using System.Linq;
using DAL.Contracts.DTO;
using DAL.Contracts.Repositories;
using DAL.Domains.Entities;

namespace DAL.Domains.Repositories
{
    public class VehicleRepository : BaseRepository<Entities.Vehicle>, IVehicleRepository
    {
        public int CreateVehicle(IVehicle model)
        {
            return base.Create(DTOToEntity(model));
        }

        public void DeleteVehicle(int id)
        {
            base.Delete(id);
        }

        public IEnumerable<IVehicle> ReadVehicles(int skip, int take)
        {
            var selected = base.Read(x => true, skip, take);
            return selected.Select(x => EntityToDTO(x));
        }

        public IEnumerable<IVehicle> SearchVehicles(string keyword, double less, int skip, int take)
        {
            var selected = base.Read(x => x.Brand.ToLower().Contains(keyword)
            && x.Value < less, skip, take);
            return selected.Select(x => EntityToDTO(x));
        }

        public IVehicle ReadVehicleById(int id)
        {
            var entity = base.Read(x => x.Id == id, 0, 1).FirstOrDefault();
            return entity == null ? null : EntityToDTO(entity);
        }

        public void UpdateVehicle(IVehicle model)
        {
            base.Update(DTOToEntity(model));
        }

        private IVehicle EntityToDTO(Entities.Vehicle item)
        {
            return new DTO.Vehicle
            {
                Id = item.Id,
                AmountOfSeats = item.AmountOfSeats,
                Brand = item.Brand,
                Carrying = item.Carrying,
                CityOfRegistration = item.CityOfRegistration,
                Color = item.Color,
                LicensePlate = item.LicensePlate,
                Number = item.Number,
                Type = item.Type,
                Value = item.Value,
                Volume = item.Volume,
                Year = item.Year
            };
        }

        private Entities.Vehicle DTOToEntity(IVehicle item)
        {
            return new Entities.Vehicle
            {
                Id = item.Id,
                AmountOfSeats = item.AmountOfSeats,
                Brand = item.Brand,
                Carrying = item.Carrying,
                CityOfRegistration = item.CityOfRegistration,
                Color = item.Color,
                LicensePlate = item.LicensePlate,
                Number = item.Number,
                Type = item.Type,
                Value = item.Value,
                Volume = item.Volume,
                Year = item.Year
            };
        }

        protected override void MapForUpdate(Vehicle selected, Vehicle entity)
        {
            if (selected == null || entity == null)
                return;
            selected.Id = entity.Id;
            selected.AmountOfSeats = entity.AmountOfSeats;
            selected.Brand = entity.Brand;
            selected.Carrying = entity.Carrying;
            selected.CityOfRegistration = entity.CityOfRegistration;
            selected.Color = entity.Color;
            selected.LicensePlate = entity.LicensePlate;
            selected.Number = entity.Number;
            selected.Type = entity.Type;
            selected.Value = entity.Value;
            selected.Volume = entity.Volume;
            selected.Year = entity.Year;
        }
    }
}
