﻿using System.Collections.Generic;
using System.Linq;
using DAL.Contracts.DTO;
using DAL.Contracts.Repositories;
using DAL.Domains.Entities;

namespace DAL.Domains.Repositories
{
    public class NodeRepository : BaseRepository<Entities.Node>, INodeRepository
    {
        public int CreateNode(INode model)
        {
            return base.Create(DTOToEntity(model));
        }

        public void DeleteNode(int id)
        {
            base.Delete(id);
        }

        public IEnumerable<INode> ReadNodes(int skip, int take)
        {
            var selected = base.Read(x => true, skip, take);
            return selected.Select(x => EntityToDTO(x));
        }

        public INode ReadNodeById(int id)
        {
            var entity = base.Read(x => x.Id == id, 0, 1).FirstOrDefault();
            return entity == null ? null : EntityToDTO(entity);
        }

        public void UpdateNode(INode model)
        {
            base.Update(DTOToEntity(model));
        }

        private INode EntityToDTO(Entities.Node item)
        {
            return new DTO.Node
            {
                Id = item.Id,
                CategoryId = item.CategoryId,
                ClientId = item.ClientId,
                ContractId = item.ContractId,
                VehicleId = item.VehicleId
            };
        }

        private Entities.Node DTOToEntity(INode item)
        {
            return new Entities.Node
            {
                Id = item.Id,
                CategoryId = item.CategoryId,
                ClientId = item.ClientId,
                ContractId = item.ContractId,
                VehicleId = item.VehicleId
            };
        }

        protected override void MapForUpdate(Node selected, Node entity)
        {
            if (selected == null || entity == null)
                return;
            selected.Id = entity.Id;
            selected.CategoryId = entity.CategoryId;
            selected.ClientId = entity.ClientId;
            selected.ContractId = entity.ContractId;
            selected.VehicleId = entity.VehicleId;
        }
    }
}
