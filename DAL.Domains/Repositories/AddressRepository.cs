﻿using System.Collections.Generic;
using System.Linq;
using DAL.Contracts.DTO;
using DAL.Contracts.Repositories;
using DAL.Domains.Entities;

namespace DAL.Domains.Repositories
{
    public class AddressRepository : BaseRepository<Entities.Address>, IAddressRepository
    {
        public int CreateAddress(IAddress model)
        {
            return base.Create(DTOToEntity(model));
        }

        public void DeleteAddress(int id)
        {
            base.Delete(id);
        }

        public IEnumerable<IAddress> ReadAddresses(int skip, int take)
        {
            var selected = base.Read(x => true, skip, take);
            return selected.Select(x => EntityToDTO(x));
        }

        public IAddress ReadAddressById(int id)
        {
            var entity = base.Read(x => x.Id == id, 0, 1).FirstOrDefault();
            return entity == null ? null : EntityToDTO(entity);
        }

        public void UpdateAddress(IAddress model)
        {
            base.Update(DTOToEntity(model));
        }

        private IAddress EntityToDTO(Entities.Address item)
        {
            return new DTO.Address
            {
                Building = item.Building,
                City = item.City,
                Country = item.Country,
                FlatNumber = item.FlatNumber,
                Id = item.Id,
                PostCode = item.PostCode,
                Street = item.Street
            };
        }

        private Entities.Address DTOToEntity(IAddress item)
        {
            return new Entities.Address
            {
                Building = item.Building,
                City = item.City,
                Country = item.Country,
                FlatNumber = item.FlatNumber,
                Id = item.Id,
                PostCode = item.PostCode,
                Street = item.Street
            };
        }

        protected override void MapForUpdate(Address selected, Address entity)
        {
            if (selected == null || entity == null)
                return;
            selected.Building = entity.Building;
            selected.City = entity.City;
            selected.Country = entity.Country;
            selected.FlatNumber = entity.FlatNumber;
            selected.Id = entity.Id;
            selected.PostCode = entity.PostCode;
            selected.Street = entity.Street;
        }
    }
}
