﻿using System.ComponentModel.DataAnnotations;

namespace DAL.Domains.Entities
{
    public class Address : EntityBase
    {
        [Required]
        public virtual string Country { get; set; }

        [Required]
        public virtual string City { get; set; }

        [Required]
        public virtual string Street { get; set; }

        [Required]
        public virtual int Building { get; set; }

        public virtual int? FlatNumber { get; set; }

        [Required]
        public virtual int PostCode { get; set; }
    }
}
