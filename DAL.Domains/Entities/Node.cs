﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Domains.Entities
{
    public class Node : EntityBase
    {
        public virtual int ClientId { get; set; }

        public virtual int VehicleId { get; set; }

        public virtual int ContractId { get; set; }

        public virtual int CategoryId { get; set; }

        [ForeignKey("ClientId")]
        public virtual Client Client { get; set; }

        [ForeignKey("VehicleId")]
        public virtual Vehicle Vehicle { get; set; }

        [ForeignKey("ContractId")]
        public virtual Contract Contract { get; set; }

        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }
    }
}
