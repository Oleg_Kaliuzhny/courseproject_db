﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Domains.Entities
{
    public class Category : EntityBase
    {
        [Required]
        [MinLength(1)]
        [MaxLength(255)]
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
