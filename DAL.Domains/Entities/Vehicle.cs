﻿using System.ComponentModel.DataAnnotations;

namespace DAL.Domains.Entities
{
    public class Vehicle : EntityBase
    {
        [Required]
        public virtual string Brand { get; set; }
        
        public virtual int Number { get; set; }

        public virtual int Year { get; set; }

        [Required]
        public virtual string LicensePlate { get; set; }

        public virtual double Value { get; set; }

        [Required]
        public virtual string Color { get; set; }
        
        public virtual double Volume { get; set; }
        
        public virtual int AmountOfSeats { get; set; }
        
        public virtual double Carrying { get; set; }

        [Required]
        public virtual string CityOfRegistration { get; set; }

        [Required]
        public virtual string Type { get; set; }
    }
}
