﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Domains.Entities
{
    public class Client : EntityBase
    {
        [Required]
        public virtual string FirstName { get; set; }

        [Required]
        public virtual string LastName { get; set; }

        [Required]
        public virtual string MiddleName { get; set; }

        [Required]
        public virtual int TaxNumber { get; set; }
        
        public virtual bool IsPhysicalPerson { get; set; }

        [Required]
        public virtual string Passport { get; set; }
        
        public virtual int AddressId { get; set; }

        [Required]
        [MinLength(5)]
        [MaxLength(20)]
        public virtual string Phone { get; set; }

        [Required]
        [MaxLength(30)]
        public virtual string DriversLicenseNumber { get; set; }

        public virtual int DriversExpirience { get; set; }

        [ForeignKey("AddressId")]
        public virtual Address Address { get; set; }
    }
}
