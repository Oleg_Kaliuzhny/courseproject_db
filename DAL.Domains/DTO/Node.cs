﻿using DAL.Contracts.DTO;

namespace DAL.Domains.DTO
{
    public class Node : DTOBase, INode
    {
        public virtual int ClientId { get; set; }

        public virtual int VehicleId { get; set; }

        public virtual int ContractId { get; set; }

        public virtual int CategoryId { get; set; }
    }
}
