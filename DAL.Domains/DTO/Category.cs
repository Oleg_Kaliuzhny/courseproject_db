﻿using DAL.Contracts.DTO;

namespace DAL.Domains.DTO
{
    public class Category : DTOBase, ICategory
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
