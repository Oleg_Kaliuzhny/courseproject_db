﻿using DAL.Contracts.DTO;

namespace DAL.Domains.DTO
{
    public class Client : DTOBase, IClient
    {
        public virtual string FirstName { get; set; }
        
        public virtual string LastName { get; set; }
        
        public virtual string MiddleName { get; set; }
        
        public virtual int TaxNumber { get; set; }
        
        public virtual bool IsPhysicalPerson { get; set; }
        
        public virtual string Passport { get; set; }
        
        public virtual int AddressId { get; set; }
        
        public virtual string Phone { get; set; }
        
        public virtual string DriversLicenseNumber { get; set; }

        public virtual int DriversExpirience { get; set; }
    }
}
