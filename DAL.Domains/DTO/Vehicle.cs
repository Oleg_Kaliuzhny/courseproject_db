﻿using DAL.Contracts.DTO;

namespace DAL.Domains.DTO
{
    public class Vehicle : DTOBase, IVehicle
    {
        public virtual string Brand { get; set; }
        
        public virtual int Number { get; set; }

        public virtual int Year { get; set; }
        
        public virtual string LicensePlate { get; set; }

        public virtual double Value { get; set; }

        public virtual string Color { get; set; }
        
        public virtual double Volume { get; set; }
        
        public virtual int AmountOfSeats { get; set; }
        
        public virtual double Carrying { get; set; }
        
        public virtual string CityOfRegistration { get; set; }

        public virtual string Type { get; set; }
    }
}
