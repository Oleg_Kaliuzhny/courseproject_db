﻿using DAL.Contracts.DTO;

namespace DAL.Domains.DTO
{
    public abstract class DTOBase : IDTOBase
    {
        public virtual int Id { get; set; }
    }
}
