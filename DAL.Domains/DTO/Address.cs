﻿using DAL.Contracts.DTO;

namespace DAL.Domains.DTO
{
    public class Address : DTOBase, IAddress
    {
        public virtual string Country { get; set; }
        
        public virtual string City { get; set; }
        
        public virtual int Building { get; set; }

        public virtual string Street { get; set; }

        public virtual int? FlatNumber { get; set; }
        
        public virtual int PostCode { get; set; }
    }
}
