namespace DAL.Domains.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Inital : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Country = c.String(nullable: false),
                        City = c.String(nullable: false),
                        Street = c.String(nullable: false),
                        Building = c.Int(nullable: false),
                        FlatNumber = c.Int(),
                        PostCode = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 255),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        MiddleName = c.String(nullable: false),
                        TaxNumber = c.Int(nullable: false),
                        IsPhysicalPerson = c.Boolean(nullable: false),
                        Passport = c.String(nullable: false),
                        AddressId = c.Int(nullable: false),
                        Phone = c.String(nullable: false, maxLength: 20),
                        DriversLicenseNumber = c.String(nullable: false, maxLength: 30),
                        DriversExpirience = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Addresses", t => t.AddressId, cascadeDelete: true)
                .Index(t => t.AddressId);
            
            CreateTable(
                "dbo.Contracts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ContractNumber = c.Int(nullable: false),
                        IsPhysicalPerson = c.Boolean(nullable: false),
                        TaxNumber = c.Int(nullable: false),
                        Value = c.Double(nullable: false),
                        VehicleOwnerName = c.String(),
                        Brand = c.String(),
                        Franchise = c.Double(nullable: false),
                        BeginningDate = c.DateTime(nullable: false),
                        FinishingDate = c.DateTime(nullable: false),
                        RegistrationDate = c.DateTime(nullable: false),
                        Sticker = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Nodes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClientId = c.Int(nullable: false),
                        VehicleId = c.Int(nullable: false),
                        ContractId = c.Int(nullable: false),
                        CategoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CategoryId, cascadeDelete: true)
                .ForeignKey("dbo.Clients", t => t.ClientId, cascadeDelete: true)
                .ForeignKey("dbo.Contracts", t => t.ContractId, cascadeDelete: true)
                .ForeignKey("dbo.Vehicles", t => t.VehicleId, cascadeDelete: true)
                .Index(t => t.ClientId)
                .Index(t => t.VehicleId)
                .Index(t => t.ContractId)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.Vehicles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Brand = c.String(nullable: false),
                        Number = c.Int(nullable: false),
                        Year = c.Int(nullable: false),
                        LicensePlate = c.String(nullable: false),
                        Value = c.Double(nullable: false),
                        Color = c.String(nullable: false),
                        Volume = c.Double(nullable: false),
                        AmountOfSeats = c.Int(nullable: false),
                        Carrying = c.Double(nullable: false),
                        CityOfRegistration = c.String(nullable: false),
                        Type = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Nodes", "VehicleId", "dbo.Vehicles");
            DropForeignKey("dbo.Nodes", "ContractId", "dbo.Contracts");
            DropForeignKey("dbo.Nodes", "ClientId", "dbo.Clients");
            DropForeignKey("dbo.Nodes", "CategoryId", "dbo.Categories");
            DropForeignKey("dbo.Clients", "AddressId", "dbo.Addresses");
            DropIndex("dbo.Nodes", new[] { "CategoryId" });
            DropIndex("dbo.Nodes", new[] { "ContractId" });
            DropIndex("dbo.Nodes", new[] { "VehicleId" });
            DropIndex("dbo.Nodes", new[] { "ClientId" });
            DropIndex("dbo.Clients", new[] { "AddressId" });
            DropTable("dbo.Vehicles");
            DropTable("dbo.Nodes");
            DropTable("dbo.Contracts");
            DropTable("dbo.Clients");
            DropTable("dbo.Categories");
            DropTable("dbo.Addresses");
        }
    }
}
