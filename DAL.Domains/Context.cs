﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using DAL.Domains.Entities;

namespace DAL.Domains
{
    public class Context : DbContext
    {
        public Context()
          : base("Insurance")
        {

        }

        public DbSet<Address> Addresses { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Client> Clients { get; set; }

        public DbSet<Contract> Contracts { get; set; }

        public DbSet<Node> Nodes { get; set; }

        public DbSet<Vehicle> Vehicles { get; set; }
    }
}
